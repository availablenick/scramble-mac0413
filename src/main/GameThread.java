package main;

public class GameThread implements Runnable {
  	private GameCanvas canvas;
	private Thread thread;
	private boolean isRunning = false;
	
	public GameThread(GameCanvas canvas) {
    	this.canvas = canvas;
	}
	
	public synchronized void start() {
		this.thread = new Thread(this);
		this.thread.start();
		isRunning = true;
	}
	
	public synchronized void stop() {
		try {
			this.thread.join();
			isRunning = false;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void run() {
		this.canvas.requestFocus();
		long lastTime = System.nanoTime();
		double amountOfTicks = 60.0;
		double ns = 1000000000.0 / amountOfTicks;
		double delta = 0;
		long timer = System.currentTimeMillis();
		while (isRunning) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while (delta >= 1) {
				this.canvas.tick();
				delta--;
			}

			if (isRunning) {
				this.canvas.render();
			}

			if ((System.currentTimeMillis() - timer) > 1000) {
				timer += 1000;
			}
		}

		stop();
	}
}
