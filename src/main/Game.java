package main;

import audio.AudioPlayer;
import audio.PitchDecoratorAudioPlayer;
import audio.VolumeDecoratorAudioPlayer;
import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;
import objects.ShootingContext;
import objects.ShootingNormal;
import screen.MainMenuScreen;

public class Game {
	public static final int WIDTH = 640;
	public static final int HEIGHT = 480;

	public static void main(String[] args) {
		AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
		ShootingContext.setInstance(new ShootingContext(new ShootingNormal()));
		initAudioPlayers();
		GameCanvas canvas = new GameCanvas(MainMenuScreen.getInstance());
		GameThread thread = new GameThread(canvas);
		new Window(WIDTH, HEIGHT, "SCRAMBLE", canvas);
		thread.start();
	}
	
	public static void initAudioPlayers() {
		AudioPlayer audioPlayer = AudioPlayer.getInstance();
		VolumeDecoratorAudioPlayer volumeAudioPlayer = VolumeDecoratorAudioPlayer.getInstance();
		volumeAudioPlayer.setAudioPlayer(audioPlayer);
		PitchDecoratorAudioPlayer pitchAudioPlayer = PitchDecoratorAudioPlayer.getInstance();
		pitchAudioPlayer.setAudioPlayer(volumeAudioPlayer);
		pitchAudioPlayer.playBackgroundMusic();
	}
}
