package main;

import java.util.ArrayList;
import java.util.List;

import objects.*;

public class HandlerSnapshot {
	private FuelBarObject fuelBar;
	private ScoreObject scoreObject;
	private PlayerObject player;
	private GameObject stageLastObject;
	private List<GameObject> objects;
	private List<GameObject> visibleObjects;
	private List<GameObject> objectsToRemoveOnCurrentTick;

	public void update(Handler handler) {
		this.fuelBar = new FuelBarObject(handler.fuelBar);
		this.scoreObject = new ScoreObject(handler.scoreObject);
		this.player = new PlayerObject(handler.player);
		this.player.setVelocityX(0);
		this.player.setVelocityY(0);
		this.objects = new ArrayList<>();

		for (int i = 0; i < handler.objects.size(); i++) {
			GameObject clone = handler.objects.get(i).clone();
			this.objects.add(clone);
			if (handler.objects.get(i) == handler.stageLastObject) {
				this.stageLastObject = clone;
			}
		}
	}

	public void restore(Handler handler) {
		handler.removeAll();

		handler.fuelBar = new FuelBarObject(this.fuelBar);
		handler.fuelBar.resetFuelAmount();
		handler.scoreObject = new ScoreObject(this.scoreObject);
		handler.player = new PlayerObject(this.player);
		handler.objects = new ArrayList<>();

		for (int i = 0; i < this.objects.size(); i++) {
			GameObject object = this.objects.get(i);

			if (object instanceof PlayerObject) {
				handler.objects.add(handler.player);
			} else {
				GameObject clone = object.clone();
				handler.objects.add(clone);
				if (object == this.stageLastObject) {
					handler.stageLastObject = clone;
				}
			}
		}
	}
}
