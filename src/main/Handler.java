package main;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import factories.AbstractObjectsFactory;
import screen.PlayingScreen;
import stages.Stage1;
import objects.*;
import screen.GameplayKeyListener;

public class Handler {
	private static Handler instance = null;
	public HandlerSnapshot snapshot;
	public FuelBarObject fuelBar;
	public ScoreObject scoreObject;
	public PlayerObject player;
	public LifeCounterObject lifeCounter;
	public List<GameObject> objects;
	public List<GameObject> visibleObjects;
	public List<GameObject> objectsToRemoveOnCurrentTick;
	public boolean isStageFinished;
	public GameObject stageLastObject;

	public Handler() {
		this.snapshot = new HandlerSnapshot();
		this.objects = new ArrayList<>();

		this.objectsToRemoveOnCurrentTick = new ArrayList<>();
		this.visibleObjects = new ArrayList<>();
		this.isStageFinished = false;
		this.lifeCounter = new LifeCounterObject(ID.LifeCounterObject, 15, 15, 0, 0, ""); 
		this.fuelBar = (FuelBarObject) AbstractObjectsFactory.getInstance().createObject("FuelBar");
		this.scoreObject = (ScoreObject) AbstractObjectsFactory.getInstance().createObject("Score");
		fuelBar.setX(560);
		fuelBar.setY(10);
	}
	
	public static Handler getInstance() {
		return instance ;
	}
	
	public static void setInstance(Handler handler) {
		instance = handler;
	}
	
	public void tick() {
		this.objectsToRemoveOnCurrentTick.clear();
		this.visibleObjects.clear();
		GameObject object = null;

		for (int i = 0; i < this.objects.size(); i++) {
			object = this.objects.get(i);
			object.tick();

			int x = object.getX();
			int y = object.getY();
			int velX = object.getVelocityX();
			int velY = object.getVelocityY();
			int width = object.getWidth();
			int height = object.getHeight();

			if ((x > Game.WIDTH + 100 && velX >= 0) ||
				(x + width < -100 && velX <= 0) ||
				(y + height < -100 && velY <= 0) ||
				(y > Game.HEIGHT + 100 && velY >=0)) {
				this.objectsToRemoveOnCurrentTick.add(object);
			} else if (x + width > 0 && x < Game.WIDTH && y + height > 0 && y < Game.HEIGHT) {
				this.visibleObjects.add(object);
			}
			this.checkExplosionFaded(object);

		}

		this.handleCollisions();

		boolean wasCheckpointRemoved = false;
		
		for (int i = 0; i < this.objectsToRemoveOnCurrentTick.size(); i++) {
			GameObject objectToRemove = this.objectsToRemoveOnCurrentTick.get(i);
			if (objectToRemove.equals(stageLastObject)) {
				this.isStageFinished = true;
			}
			this.removeObject(objectToRemove);
			if (objectToRemove instanceof CheckpointObject) {
				wasCheckpointRemoved = true;
			}
		}
		if (wasCheckpointRemoved) {
			this.snapshot.update(this);
		}

		this.fuelBar.tick();
		if (this.fuelBar.getFuelAmount() <= 0) {
			this.snapshot.restore(this);
			this.lifeCounter.decrementLifeCount();
		}
	}

	public void render(Graphics g) {
		for (int i = 0; i < this.visibleObjects.size(); i++) {
			this.visibleObjects.get(i).render(g);
		}

		this.lifeCounter.render(g);
		this.fuelBar.render(g);
		this.scoreObject.render(g);
	}

	public void addObject(GameObject object) {
		this.objects.add(object);
	}

	public void removeObject(GameObject object) {
		this.visibleObjects.remove(object);
		this.objects.remove(object);
	}

	public void removeAll() {
		for (int i = 0; i < this.objects.size(); i++) {
			this.removeObject(this.objects.get(i));
		}
	}

	public GameObject getObjectAt(int index) {
		return this.objects.get(index);
	}

	public int countObjects() {
		return this.objects.size();
	}

	public PlayerObject getPlayer() {
		return player;
	}

	public void setPlayer(PlayerObject player) {
		this.player = player;
	}

	private boolean collide(GameObject firstObject, GameObject secondObject) {
		Rectangle firstObjectOutline = (Rectangle) firstObject.getBounds();
		return firstObjectOutline.intersects((Rectangle) secondObject.getBounds());
	}

	private void checkExplosionFaded(GameObject object) {
		if (object instanceof ExplosionObject) {
			if (((ExplosionObject) object).getAlpha() <= 0) this.objectsToRemoveOnCurrentTick.add(object);
		}
	}

	private ExplosionObject createExplosionObject(int xFirstObject, int yFirstObject, int xSecondObject, int ySecondObject) {
		ExplosionObject explosion = (ExplosionObject) AbstractObjectsFactory.getInstance().createObject("Explosion");
		explosion.setX((xFirstObject + xSecondObject) / 2);
		explosion.setY((yFirstObject + ySecondObject) / 2);
		return explosion;
	}

	private void resolveCollision(GameObject firstObject, GameObject secondObject, CollisionResult result) {
		switch (result) {
			case TakePlayerLife:
				this.snapshot.restore(this);
				this.lifeCounter.decrementLifeCount();
				break;
			case SetCheckpoint:
				this.objectsToRemoveOnCurrentTick.add(firstObject);
				break;
			case RemoveFirstObject:
				this.objectsToRemoveOnCurrentTick.add(firstObject);
				break;
			case RemoveSecondObject:
				this.objectsToRemoveOnCurrentTick.add(secondObject);
				break;
			case RefillFuelTank:
				((FuelBarObject) this.fuelBar).resetFuelAmount();
				this.addObject(this.createExplosionObject(firstObject.getX(), firstObject.getY(), secondObject.getX(), secondObject.getY()));
				this.objectsToRemoveOnCurrentTick.add(firstObject);
				this.objectsToRemoveOnCurrentTick.add(secondObject);
				break;
			case ExplodeObjects:
				if (firstObject instanceof Scorable) {
					scoreObject.increaseScoreBy(((Scorable) firstObject).getScoreValue());
				}
				if (secondObject instanceof Scorable) {
					scoreObject.increaseScoreBy(((Scorable) secondObject).getScoreValue());
				}
				this.addObject(this.createExplosionObject(firstObject.getX(), firstObject.getY(), secondObject.getX(), secondObject.getY()));
				this.objectsToRemoveOnCurrentTick.add(firstObject);
				this.objectsToRemoveOnCurrentTick.add(secondObject);
				break;
			case IncreasePlayerAgility:
				if (firstObject instanceof BonusObject) {
					this.objectsToRemoveOnCurrentTick.add(firstObject);
				}
				else {
					this.objectsToRemoveOnCurrentTick.add(secondObject);
				}
				this.player.setBonusVelocity(2);
				break;
			default:
				break;
		}
	}

	private void handleCollisions() {
		for (int i = 0; i < this.visibleObjects.size() - 1; i++) {
			GameObject firstObject = this.visibleObjects.get(i);
			if (this.objectsToRemoveOnCurrentTick.contains(firstObject)) continue;

			for (int j = i + 1; j < this.visibleObjects.size(); j++) {
				GameObject secondObject = this.visibleObjects.get(j);
				if (this.objectsToRemoveOnCurrentTick.contains(secondObject)) continue;

				if (this.collide(firstObject, secondObject)) {
					this.resolveCollision(firstObject, secondObject, firstObject.collideWith(secondObject));
				}
			}
		}
	}
}
