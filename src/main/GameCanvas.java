package main;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;

import factories.AbstractObjectsFactory;
import objects.GameBackground;
import screen.AudioMenuScreen;
import screen.GameScreen;
import screen.MainMenuScreen;
import screen.PitchMenuScreen;
import screen.PlayingScreen;
import screen.ShootingMenu;
import screen.StageSelectMenuScreen;
import screen.ThemesMenuScreen;
import screen.VolumeMenuScreen;

public class GameCanvas extends Canvas {
  	private static final long serialVersionUID = -4284358379897487885L;
	private GameBackground background;
	private GameScreen screen;

	public GameCanvas(GameScreen initialScreen) {
		this.initScreens();
		this.updateBackground();
		this.screen = initialScreen;
		this.screen.setUp();
	}

	public void render() {
		BufferStrategy bs = this.getBufferStrategy();
		if (bs == null) {
			this.createBufferStrategy(3);
			return;
		}

		Graphics g = bs.getDrawGraphics();

		this.screen.render(g, this.background);

		g.dispose();
		bs.show();
	}

	public void tick() {
		this.screen.tick();
	}

	public void updateBackground() {
		this.background = (GameBackground) AbstractObjectsFactory.getInstance().createObject("GameBackground");
	}

	public void setScreen(GameScreen screen) {
		this.screen.tearDown();
		this.screen = screen;
		this.screen.setUp();
	}

	private void initScreens() {
		MainMenuScreen.getInstance().setCanvas(this);
		StageSelectMenuScreen.getInstance().setCanvas(this);
		ShootingMenu.getInstance().setCanvas(this);
		AudioMenuScreen.getInstance().setCanvas(this);
		ThemesMenuScreen.getInstance().setCanvas(this);
		PlayingScreen.getInstance().setCanvas(this);
		VolumeMenuScreen.getInstance().setCanvas(this);
		PitchMenuScreen.getInstance().setCanvas(this);
	}
}
