package main;

public enum ID {
	Background(),
	CheckpointObject(),
	EnemyObject(),
	ExplosionObject(),
	ExplosiveObject(),
	FireballObject(),
	FuelBarObject(),
	FuelContainerObject(),
	LifeCounterObject(),
	LifeObject(),
	PlayerObject(),
	ProjectileObject(),
	ScoreObject(),
	SpaceshipObject(),
	WallObject(), 
	BonusObject(),
}
