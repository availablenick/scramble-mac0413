package stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.GameObject;

public class Stage1 extends GameStage {
	private final Map<String, List<Integer[]>> objectTypeToParamsMap;

	public Stage1() {
		this.handler = new Handler();
		this.hasNextStage = true;
		this.stageName = "Stage 1";

		List<Integer[]> bonusParamsList = new ArrayList<>();
		List<Integer[]> checkpointParamsList = new ArrayList<>();
		List<Integer[]> enemyParamsList = new ArrayList<>();
		List<Integer[]> fuelParamsList = new ArrayList<>();
		
		objectTypeToParamsMap = new HashMap<>();
		objectTypeToParamsMap.put("Bonus", bonusParamsList);
		objectTypeToParamsMap.put("Checkpoint", checkpointParamsList);
		objectTypeToParamsMap.put("Enemy", enemyParamsList);
		objectTypeToParamsMap.put("FuelContainer", fuelParamsList);
		Random r = new Random();

		bonusParamsList.add(new Integer[]{3000, Game.HEIGHT - 120, -4, 0});
		
		for (int i=0;i<10;i++) enemyParamsList.add(new Integer[]{1000 + i*100, Game.HEIGHT - 80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{2000 + i*100, Game.HEIGHT - 80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{2800 + i*100, Game.HEIGHT - 160, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{3500 + i*100, Game.HEIGHT - 80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{4200 + i*100, Game.HEIGHT - 192, -3, r.nextInt(2)});
		
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{5000 + i*100, Game.HEIGHT -80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{5800 + i*100, Game.HEIGHT - 192, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{6500 + i*100, Game.HEIGHT -80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{7200 + i*100, Game.HEIGHT - 192, -3, r.nextInt(2)});
		
		fuelParamsList.add(new Integer[]{2060, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{5060, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{8060, Game.HEIGHT - 80, -3, 0});
		checkpointParamsList.add(new Integer[]{2000, Game.HEIGHT/2 - 30, -3, 0});
		checkpointParamsList.add(new Integer[]{4500, Game.HEIGHT/2 - 30, -3, 0});
	}

	public GameStage nextStage() {
		return new Stage2();
	}

	public void setUp() {
		this.setUpPlayer();

		for (Map.Entry<String, List<Integer[]>> entry : objectTypeToParamsMap.entrySet()) {
			String objectType = entry.getKey();
			for (Integer[] params : entry.getValue()) {
				GameObject object = AbstractObjectsFactory.getInstance().createObject(objectType);
				object.setX(params[0]);
				object.setY(params[1]);
				object.setVelocityX(params[2]);
				object.setVelocityY(params[3]);
				this.handler.addObject(object);
			}
		}
		
		this.insertTrapezoidalWall(2632, 2760, 3272, 3400, Game.HEIGHT - 128, 0, "Bottom");
		
		this.insertTrapezoidalWall(3952, 4112, 4720, 4880, Game.HEIGHT - 160, 0, "Bottom");
		
		this.insertTrapezoidalWall(5568, 5728, 6304, 6464, Game.HEIGHT - 160, 0, "Bottom");

		this.insertTrapezoidalWall(6952, 7112, 7720, 7880, Game.HEIGHT - 160, 0, "Bottom");
		
		this.handler.stageLastObject = this.handler.getObjectAt(this.handler.countObjects() - 1);

	}
}
