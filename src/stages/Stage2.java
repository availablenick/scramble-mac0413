package stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.GameObject;

public class Stage2 extends GameStage {
	private final Map<String, List<Integer[]>> objectTypeToParamsMap;

	public Stage2() {
		this.handler = new Handler();
		this.hasNextStage = true;
		this.stageName = "Stage 2";

		List<Integer[]> bonusParamsList = new ArrayList<>();
		List<Integer[]> checkpointParamsList = new ArrayList<>();
		List<Integer[]> enemyParamsList = new ArrayList<>();
		List<Integer[]> fuelParamsList = new ArrayList<>();
		List<Integer[]> spaceshipParamsList = new ArrayList<>();
		
		objectTypeToParamsMap = new HashMap<>();
		objectTypeToParamsMap.put("Bonus", bonusParamsList);
		objectTypeToParamsMap.put("Checkpoint", checkpointParamsList);
		objectTypeToParamsMap.put("Enemy", enemyParamsList);
		objectTypeToParamsMap.put("FuelContainer", fuelParamsList);
		objectTypeToParamsMap.put("Spaceship", spaceshipParamsList);
		Random r = new Random();

		
		bonusParamsList.add(new Integer[]{3000, Game.HEIGHT - 120, -4, 0});
		
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{1000 + i*100, Game.HEIGHT -80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{1800 + i*100, Game.HEIGHT - 160, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{2500 + i*100, Game.HEIGHT -80, -3, r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{3200 + i*100, Game.HEIGHT - 192, -3, r.nextInt(2)});

		for (int i=0;i<10;i++) spaceshipParamsList.add(new Integer[]{4000 + i*100, Game.HEIGHT/2 -70*r.nextInt(3) , -3,0});
		for (int i=0;i<31;i++) enemyParamsList.add(new Integer[]{5200 + i*100, Game.HEIGHT - 192, -3, r.nextInt(2)});

		for (int i=0;i<30;i++) spaceshipParamsList.add(new Integer[]{5200 + i*100, Game.HEIGHT/2 -70*r.nextInt(3) , -3,0});

		for (int i=0;i<35;i++) enemyParamsList.add(new Integer[]{8400 + i*75, Game.HEIGHT - 192, -3, r.nextInt(2)});
		for (int i=0;i<30;i++) spaceshipParamsList.add(new Integer[]{8400 + i*75, Game.HEIGHT/2 -70*r.nextInt(3) , -3,0});
		
		
		fuelParamsList.add(new Integer[]{1060, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{3060, Game.HEIGHT - 170, -3, 0});
		fuelParamsList.add(new Integer[]{5090, Game.HEIGHT - 180, -3, 0});
		fuelParamsList.add(new Integer[]{8060, Game.HEIGHT - 190, -3, 0});
		
		checkpointParamsList.add(new Integer[]{2000, Game.HEIGHT/2 - 30, -3, 0});
		checkpointParamsList.add(new Integer[]{7500, Game.HEIGHT/2 - 30, -3, 0});
		
		
	}

	@Override
	public Handler getHandler() {
		return this.handler;
	}

	public GameStage nextStage() {
		return new Stage3();
	}

	@Override
	public void setUp() {
		this.setUpPlayer();

		for (Map.Entry<String, List<Integer[]>> entry : objectTypeToParamsMap.entrySet()) {
			String objectType = entry.getKey();
			for (Integer[] params : entry.getValue()) {
				GameObject object = AbstractObjectsFactory.getInstance().createObject(objectType);
				object.setX(params[0]);
				object.setY(params[1]);
				object.setVelocityX(params[2]);
				object.setVelocityY(params[3]);
				this.handler.addObject(object);
			}
		}
		
		this.insertTrapezoidalWall(1632, 1760, 2272, 2400, Game.HEIGHT - 128, 0, "Bottom");
		
		this.insertTrapezoidalWall(2952, 3112, 3720, 3880, Game.HEIGHT - 160, 0, "Bottom");
		
		this.insertTrapezoidalWall(4971, 5131, 11051, 11211, Game.HEIGHT - 160, 0, "Bottom");
		
		this.handler.stageLastObject = this.handler.getObjectAt(this.handler.countObjects() - 1);
	}
}
