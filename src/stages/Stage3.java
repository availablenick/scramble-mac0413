package stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.GameObject;

public class Stage3 extends GameStage {
	private final Map<String, List<Integer[]>> objectTypeToParamsMap;

	public Stage3() {
		this.handler = new Handler();
		this.hasNextStage = true;
		this.stageName = "Stage 3";

		
		List<Integer[]> bonusParamsList = new ArrayList<>();
		List<Integer[]> checkpointParamsList = new ArrayList<>();
		List<Integer[]> fireballParamsList = new ArrayList<>();
		List<Integer[]> fuelParamsList = new ArrayList<>();
		List<Integer[]> spaceshipParamsList = new ArrayList<>();
		
		objectTypeToParamsMap = new HashMap<>();
		
		objectTypeToParamsMap.put("Bonus", bonusParamsList);
		objectTypeToParamsMap.put("Checkpoint", checkpointParamsList);
		objectTypeToParamsMap.put("Fireball", fireballParamsList);
		objectTypeToParamsMap.put("FuelContainer", fuelParamsList);
		objectTypeToParamsMap.put("Spaceship", spaceshipParamsList);
		
		bonusParamsList.add(new Integer[]{3000, Game.HEIGHT - 320, -4, 0});

		for (int i=0;i<9;i++) fireballParamsList.add(new Integer[]{1050 + i*1200, Game.HEIGHT/4 -100, -4, 0});
		for (int i=0;i<10;i++) fireballParamsList.add(new Integer[]{805 + i*1500, Game.HEIGHT/4 - 100, -4, 0});

		for (int i=0;i<20;i++) fireballParamsList.add(new Integer[]{800 + i*500, Game.HEIGHT/4 -50, -4, 0});

		for (int i=0;i<9;i++) fireballParamsList.add(new Integer[]{1050 + i*800, Game.HEIGHT/4, -4, 0});
		for (int i=0;i<5;i++) fireballParamsList.add(new Integer[]{8500 + i*800, Game.HEIGHT/4, -4, 0});

		for (int i=0;i<20;i++) fireballParamsList.add(new Integer[]{900 + i*500, Game.HEIGHT/4 +50, -4, 0});
		for (int i=0;i<10;i++) fireballParamsList.add(new Integer[]{1200 + i*1000, Game.HEIGHT/4 +50, -4, 0});

		for (int i=0;i<20;i++) fireballParamsList.add(new Integer[]{700 + i*500, Game.HEIGHT/4 +100, -4, 0});
		
		fuelParamsList.add(new Integer[]{1000, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{3500, Game.HEIGHT - 240, -3, 0});
		fuelParamsList.add(new Integer[]{5500, Game.HEIGHT - 230, -3, 0});
		fuelParamsList.add(new Integer[]{7500, Game.HEIGHT - 230, -3, 0});
		
		checkpointParamsList.add(new Integer[]{2000, Game.HEIGHT/2 - 30, -3, 0});
		checkpointParamsList.add(new Integer[]{5700, Game.HEIGHT/2 - 30, -3, 0});
		
	}

	@Override
	public Handler getHandler() {
		return this.handler;
	}

	public GameStage nextStage() {
		return new Stage4();
	}

	@Override
	public void setUp() {
		this.setUpPlayer();

		for (Map.Entry<String, List<Integer[]>> entry : objectTypeToParamsMap.entrySet()) {
			String objectType = entry.getKey();
			for (Integer[] params : entry.getValue()) {
				GameObject object = AbstractObjectsFactory.getInstance().createObject(objectType);
				object.setX(params[0]);
				object.setY(params[1]);
				object.setVelocityX(params[2]);
				object.setVelocityY(params[3]);
				this.handler.addObject(object);
			}
		}
		
		this.insertTrapezoidalWall(1000, 1192, 12424, 12616, Game.HEIGHT - 192, 0, "Bottom");

		this.insertRectangularWall(9000, 0, 3424, 196, "Top");
		this.insertTriangularWall(12424, 0, 196, 196, "TopAscendingDiagonal");
		
		this.handler.stageLastObject = this.handler.getObjectAt(this.handler.countObjects() - 1);
	}
}
