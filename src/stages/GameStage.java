package stages;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.PlayerObject;
import objects.WallObject;
import java.lang.Math;

public abstract class GameStage {
	
	public Handler handler;
	
	public Handler getHandler() {
		return this.handler;
	}
	
	public boolean hasNextStage;
	
	public String stageName;
	
	public abstract GameStage nextStage();
	
	public abstract void setUp();

	protected void setUpPlayer() {
		PlayerObject player = (PlayerObject) AbstractObjectsFactory.getInstance().createObject("Player");

		player.setX(Game.WIDTH/2 - 32);
		player.setY(Game.HEIGHT/2 - 32);

		this.handler.addObject(player);
		this.handler.setPlayer(player);
	}
	
	public String getStageName() {
		return stageName;
	}

	public void setStageName(String stageName) {
		this.stageName = stageName;
	}

	public void insertRectangularWall(int x, int y, int width, int height, String type) {
		if (width%32 != 0) {
			if (width % 32 < 16) width -= (width%32);
			else width += (32 - width % 32);
		}
		if (height%32 != 0) {
			if (height % 32 < 16) height -= (height%32);
			else height += (32 - height % 32);
		}
		
		if (type.equals("Top")) {
		
			for (int i = x; i < x + width; i += 32) {
				for (int j = y; j < y + height; j += 32) {
					if (j + height <= 0 || j >= Game.HEIGHT) {
						continue;
					}

					WallObject wall;
					if (j == (y + height -32)) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopMiddleWall");
					}
					wall.setX(i);
					wall.setY(j);
					this.handler.addObject(wall);
				}
			}
		} else {
			for (int i = x; i < x + width; i += 32) {
				for (int j = y; j < y + height; j += 32) {
					if (j + height <= 0 || j >= Game.HEIGHT) {
						continue;
					}

					WallObject wall;
					if (j == y) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomMiddleWall");
					}
					wall.setX(i);
					wall.setY(j);
					this.handler.addObject(wall);
				}
			}
		}
	}
	
	public void insertTriangularWall(int x, int y, int width, int height, String type) {
		if (width%32 != 0) {
			if (width % 32 < 16) width -= (width%32);
			else width += (32 - width % 32);
		}
		if (height%32 != 0) {
			if (height % 32 < 16) height -= (height%32);
			else height += (32 - height % 32);
		}
		
		int tiles = 1;
		if (type.equals("TopAscendingDiagonal")) {
			for (int i = y + height; i > y; i -= 32) {
				for (int j = 0; j < tiles; j++) {
					if (i - 32 + height <= 0 || i - 32 >= Game.HEIGHT) {
						continue;
					}

					int posX = x + 32*j;
					WallObject wall;
					if (j == (tiles-1)) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopAscendingDiagonalWall");
					} else if (j == (tiles-2)) { 
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopAscendingMiddleWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopMiddleWall");
					}
					wall.setX(posX);
					wall.setY(i - 32);
					this.handler.addObject(wall);
				}
				tiles++;
			}
		} else if (type.equals("TopDescendingDiagonal")) {
			for (int i = y + height; i > y; i -= 32) {
				for (int j = 0; j < tiles; j++) {
					if (i - 32 + height <= 0 || i - 32 >= Game.HEIGHT) {
						continue;
					}

					int posX = (x + width - 32) - 32*j;
					WallObject wall;
					if (j == (tiles-1)) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopDescendingDiagonalWall");
					} else if (j == (tiles-2)) { 
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopDescendingMiddleWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("TopMiddleWall");
					}
					wall.setX(posX);
					wall.setY(i - 32);
					this.handler.addObject(wall);
				}
				tiles++;
			}
		} else if (type.equals("BottomAscendingDiagonal")) {
			for (int i = y; i < y + height; i += 32) {
				for (int j = 0; j < tiles; j++) {
					if (i + height <= 0 || i >= Game.HEIGHT) {
						continue;
					}

					int posX = (x + width - 32) - 32*j;
					WallObject wall;
					if (j == (tiles-1)) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomAscendingDiagonalWall");
					} else if (j == (tiles-2)) { 
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomAscendingMiddleWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomMiddleWall");
					}
					wall.setX(posX);
					wall.setY(i);
					this.handler.addObject(wall);
				}
				tiles++;
			}
		} else if (type.equals("BottomDescendingDiagonal")) {
			for (int i = y; i < y + height; i += 32) {
				for (int j = 0; j < tiles; j++) {
					if (i + height <= 0 || i >= Game.HEIGHT) {
						continue;
					}

					int posX = x + 32*j;
					WallObject wall;
					if (j == (tiles-1)) {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomDescendingDiagonalWall");
					} else if (j == (tiles-2)) { 
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomDescendingMiddleWall");
					} else {
						wall = (WallObject) AbstractObjectsFactory.getInstance().createObject("BottomMiddleWall");
					}
					wall.setX(posX);
					wall.setY(i);
					this.handler.addObject(wall);
				}
				tiles++;
			}
		}
	}

	public void insertTrapezoidalWall(int baseLeft, int topLeft, int topRight, int baseRight, int topY, int baseY, String type) {
		
		int height = Math.abs(baseY - topY);
		
		if (type.equals("Top")) {
			insertTriangularWall(baseLeft, baseY, topLeft - baseLeft, height, "TopDescendingDiagonal");
			insertRectangularWall(topLeft, baseY, topRight - topLeft, height, "Top");
			insertTriangularWall(topRight, baseY, baseRight - topRight, height, "TopAscendingDiagonal");
		} else if (type.equals("Bottom")) {
			insertTriangularWall(baseLeft, topY, topLeft - baseLeft, height, "BottomAscendingDiagonal");
			insertRectangularWall(topLeft, topY, topRight - topLeft, height, "Bottom");
			insertTriangularWall(topRight, topY, baseRight - topRight, height, "BottomDescendingDiagonal");
		}
	}
}
