package stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.GameObject;
import objects.PlayerObject;

public class Stage4 extends GameStage {
	private final Map<String, List<Integer[]>> objectTypeToParamsMap;

	public Stage4() {
		this.handler = new Handler();
		this.hasNextStage = true;
		this.stageName = "Stage 4";

		
		List<Integer[]> bonusParamsList = new ArrayList<>();
		List<Integer[]> checkpointParamsList = new ArrayList<>();
		List<Integer[]> enemyParamsList = new ArrayList<>();
		List<Integer[]> fuelParamsList = new ArrayList<>();
		
		objectTypeToParamsMap = new HashMap<>();
		
		objectTypeToParamsMap.put("Bonus", bonusParamsList);
		objectTypeToParamsMap.put("Checkpoint", checkpointParamsList);
		objectTypeToParamsMap.put("Enemy", enemyParamsList);
		objectTypeToParamsMap.put("FuelContainer", fuelParamsList);
		
		

		Random r = new Random();
		
		bonusParamsList.add(new Integer[]{3000, Game.HEIGHT - 320, -4, 0});

		for (int i=0;i<10;i++) enemyParamsList.add(new Integer[]{1000 + i*100, Game.HEIGHT -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{2000 + i*75, Game.HEIGHT   -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<7;i++) enemyParamsList.add(new Integer[]{2800 + i*75, Game.HEIGHT - 160, -3, 2*r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{3500 + i*75, Game.HEIGHT   -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<7;i++) enemyParamsList.add(new Integer[]{4200 + i*75, Game.HEIGHT - 192, -3, 2*r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{5000 + i*75, Game.HEIGHT   -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<7;i++) enemyParamsList.add(new Integer[]{5800 + i*75, Game.HEIGHT - 192, -3, 2*r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{6500 + i*75, Game.HEIGHT   -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<7;i++) enemyParamsList.add(new Integer[]{7200 + i*75, Game.HEIGHT - 192, -3, 2*r.nextInt(2)});
		for (int i=0;i<5;i++) enemyParamsList.add(new Integer[]{8000 + i*75, Game.HEIGHT   -80, -3, 2*r.nextInt(2)});
		for (int i=0;i<6;i++) enemyParamsList.add(new Integer[]{8800 + i*75, Game.HEIGHT - 192, -3, 2*r.nextInt(2)});
		
		fuelParamsList.add(new Integer[]{950, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{3450, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{5500, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{7900, Game.HEIGHT - 80, -3, 0});
		
		checkpointParamsList.add(new Integer[]{2000, Game.HEIGHT/2 - 30, -3, 0});
		checkpointParamsList.add(new Integer[]{5700, Game.HEIGHT/2 - 30, -3, 0});
		
		
	}

	@Override
	public Handler getHandler() {
		return this.handler;
	}

	public GameStage nextStage() {
		return new Stage5();
	}

	@Override
	public void setUp() {
		this.setUpPlayer();

		for (Map.Entry<String, List<Integer[]>> entry : objectTypeToParamsMap.entrySet()) {
			String objectType = entry.getKey();
			for (Integer[] params : entry.getValue()) {
				GameObject object = AbstractObjectsFactory.getInstance().createObject(objectType);
				object.setX(params[0]);
				object.setY(params[1]);
				object.setVelocityX(params[2]);
				object.setVelocityY(params[3]);
				this.handler.addObject(object);
			}
		}
		
		this.insertTrapezoidalWall(2657, 2785, 3297, 3425, Game.HEIGHT - 128, 0, "Bottom");
		
		this.insertTrapezoidalWall(3977, 4137, 4745, 4905, Game.HEIGHT - 160, 0, "Bottom");
		
		this.insertTrapezoidalWall(5609, 5769, 6313, 6473, Game.HEIGHT - 160, 0, "Bottom");

		this.insertTrapezoidalWall(6977, 7137, 7745, 7905, Game.HEIGHT - 160, 0, "Bottom");

		this.insertTrapezoidalWall(8609, 8769, 9313, 9473, Game.HEIGHT - 160, 0, "Bottom");
		
		this.insertRectangularWall(750, 0, 9600, 96, "Top");
		
		this.handler.stageLastObject = this.handler.getObjectAt(this.handler.countObjects() - 1);
	}
}
