package stages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import factories.AbstractObjectsFactory;
import main.Game;
import main.Handler;
import objects.GameObject;
import objects.PlayerObject;

public class Stage5 extends GameStage {
	private final Map<String, List<Integer[]>> objectTypeToParamsMap;

	public Stage5() {
		this.handler = new Handler();
		this.hasNextStage = false;
		this.stageName = "Stage 5";

		List<Integer[]> bonusParamsList = new ArrayList<>();
		List<Integer[]> fuelParamsList = new ArrayList<>();
		
		objectTypeToParamsMap = new HashMap<>();
		
		objectTypeToParamsMap.put("Bonus", bonusParamsList);
		objectTypeToParamsMap.put("FuelContainer", fuelParamsList);
		
		
		fuelParamsList.add(new Integer[]{1000, Game.HEIGHT - 80, -3, 0});
		fuelParamsList.add(new Integer[]{4500, Game.HEIGHT - 290, -3, 0});
		fuelParamsList.add(new Integer[]{8000, Game.HEIGHT - 80, -3, 0});
		
		
		
	}

	@Override
	public Handler getHandler() {
		return this.handler;
	}

	public GameStage nextStage() {
		return null;
	}

	@Override
	public void setUp() {
		this.setUpPlayer();

		GameObject lastObject = null;

		for (Map.Entry<String, List<Integer[]>> entry : objectTypeToParamsMap.entrySet()) {
			String objectType = entry.getKey();
			for (Integer[] params : entry.getValue()) {
				GameObject object = AbstractObjectsFactory.getInstance().createObject(objectType);
				object.setX(params[0]);
				object.setY(params[1]);
				object.setVelocityX(params[2]);
				object.setVelocityY(params[3]);
				this.handler.addObject(object);
			}
		}

		this.insertRectangularWall(1000, 0, 2016, 192, "Top");
		this.insertRectangularWall(2155, 0, 992, 352, "Top");
		this.insertRectangularWall(3700, Game.HEIGHT -250, 1408, 512, "Bottom");

		this.insertRectangularWall(4000, 0, 992, 160, "Top");
		this.insertRectangularWall(4990, 0, 992, 96, "Top");
		this.insertRectangularWall(5100, Game.HEIGHT -300, 1152, 512, "Bottom");
		this.insertRectangularWall(6200, Game.HEIGHT -400, 992, 512, "Bottom");

		this.insertRectangularWall(8000, 0, 992, 352, "Top");

		this.insertRectangularWall(9600, Game.HEIGHT -300, 608, 512, "Bottom");

		this.handler.stageLastObject = this.handler.getObjectAt(this.handler.countObjects() - 1);
	}
}
