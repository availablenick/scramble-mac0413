package test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.image.BufferedImage;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.WesternObjectsFactory;

import objects.GameObject;

class WesternObjectsFactoryTest {
    @BeforeAll
    static void init() {
        AbstractObjectsFactory.setInstance(WesternObjectsFactory.getInstance());
    }

    @Test
    void shouldUseSpaceBackgroundImageWhenUsingSpaceTheme() {
        GameObject background = AbstractObjectsFactory.getInstance().createObject("GameBackground");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/background.png");

        assertTrue(Helper.areImagesEqual(background.getImage(), cmp));
    }

    @Test
    void shouldUseSpaceEnemyImageWhenUsingSpaceTheme() {
        GameObject enemy = AbstractObjectsFactory.getInstance().createObject("Enemy");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/enemy.png");

        assertTrue(Helper.areImagesEqual(enemy.getImage(), cmp));
    }

    @Test
    void shouldUseSpaceExplosiveImageWhenUsingSpaceTheme() {
        GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/explosive.png");

        assertTrue(Helper.areImagesEqual(explosive.getImage(), cmp));
    }

    @Test
    void shouldUseSpaceFireballImageWhenUsingSpaceTheme() {
        GameObject fireball = AbstractObjectsFactory.getInstance().createObject("Fireball");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/fireball.png");

        assertTrue(Helper.areImagesEqual(fireball.getImage(), cmp));
    }

    @Test
    void shouldUseSpacePlayerImageWhenUsingSpaceTheme() {
        GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/player.png");

        assertTrue(Helper.areImagesEqual(player.getImage(), cmp));
    }

    @Test
    void shouldUseSpaceProjectileImageWhenUsingSpaceTheme() {
        GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/projectile.png");

        assertTrue(Helper.areImagesEqual(projectile.getImage(), cmp));
    }
    
    @Test
    void shouldUseSpaceSpaceshipImageWhenUsingSpaceTheme() {
        GameObject spaceship = AbstractObjectsFactory.getInstance().createObject("Spaceship");
        BufferedImage cmp = Helper.createBufferedImage("./img/western/spaceship.png");

        assertTrue(Helper.areImagesEqual(spaceship.getImage(), cmp));
    }
}
