package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;

import objects.CollisionResult;
import objects.GameObject;

public class ProjectileCollisionTest {
	@BeforeAll
	static void init() {
		AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
	}

	@Test
	void shouldIncreasePlayerAgilityWhenCollidingWithBonus() {
		GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
		assertTrue(projectile.collideWithBonus() == CollisionResult.IncreasePlayerAgility);
	}

	@Test
	void shouldExplodeObjectsWhenCollidingWithEnemy() {
		GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
		assertTrue(projectile.collideWithEnemy() == CollisionResult.ExplodeObjects);
	}

	@Test
	void shouldRefillFuelTankWhenCollidingWithFuelContainer() {
		GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
		assertTrue(projectile.collideWithFuelContainer() == CollisionResult.RefillFuelTank);
	}

	@Test
	void shouldExplodeObjectsWhenCollidingWithSpaceship() {
		GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
		assertTrue(projectile.collideWithSpaceship() == CollisionResult.ExplodeObjects);
	}

	@Test
	void shouldRemoveFirstObjectWhenCollidingWithWall() {
		GameObject projectile = AbstractObjectsFactory.getInstance().createObject("Projectile");
		assertTrue(projectile.collideWithWall() == CollisionResult.RemoveFirstObject);
	}
}
