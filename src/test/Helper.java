package test;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class Helper {
	static boolean areImagesEqual(BufferedImage image1, BufferedImage image2) {
        if (image1.getWidth() != image2.getWidth()) {
            return false;
        }
        
        if (image1.getHeight() != image2.getHeight()) {
            return false;
        }
        
        for (int i = 0; i < image1.getWidth(); i++) {
            for (int j = 0; j < image1.getHeight(); j++) {
                if (image1.getRGB(i, j) != image2.getRGB(i, j)) {
                    return false;
                }
            }
        }

        return true;
    }

    static BufferedImage createBufferedImage(String path) {
        BufferedImage image = null;
        try {
            image = ImageIO.read(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;
    }
}
