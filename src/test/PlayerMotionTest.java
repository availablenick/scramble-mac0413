package test;

import static org.junit.jupiter.api.Assertions.*;

import java.awt.Canvas;
import java.awt.event.KeyEvent;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;

import main.Game;
import main.Handler;

import objects.PlayerObject;

import screen.GameplayKeyListener;

class PlayerMotionTest {
    private static final int VELOCITY = 4;

    @BeforeAll
    static void init() {
        AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
    }

    @Test
    void shouldIncrementPlayerVelocityXWhenPressingD() {
        assertTrue(velocityXMatchesKeyEventResult(VELOCITY, KeyEvent.KEY_PRESSED, KeyEvent.VK_D, 'd'));
    }
    
    @Test
    void shouldDecrementPlayerVelocityXWhenPressingA() {
        assertTrue(velocityXMatchesKeyEventResult(-VELOCITY, KeyEvent.KEY_PRESSED,  KeyEvent.VK_A, 'a'));
    }
    
    @Test
    void shouldDecrementPlayerVelocityYWhenPressingW() {
        assertTrue(velocityYMatchesKeyEventResult(-VELOCITY, KeyEvent.KEY_PRESSED,  KeyEvent.VK_W, 'w'));
    }

    @Test
    void shouldIncrementPlayerVelocityYWhenPressingS() {
        assertTrue(velocityYMatchesKeyEventResult(VELOCITY, KeyEvent.KEY_PRESSED, KeyEvent.VK_S, 's'));
    }
    
    @Test
    void shouldZeroPlayerVelocityXWhenReleasingD() {
        assertTrue(velocityXMatchesKeyEventResult(0, KeyEvent.KEY_RELEASED, KeyEvent.VK_D, 'd'));
    }
    
    @Test
    void shouldZeroPlayerVelocityXWhenReleasingA() {
        assertTrue(velocityXMatchesKeyEventResult(0, KeyEvent.KEY_RELEASED, KeyEvent.VK_A, 'a'));
    }
    
    @Test
    void shouldZeroPlayerVelocityYWhenReleasingW() {
        assertTrue(velocityYMatchesKeyEventResult(0, KeyEvent.KEY_RELEASED, KeyEvent.VK_W, 'w'));
    }

    @Test
    void shouldZeroPlayerVelocityYWhenReleasingS() {
        assertTrue(velocityYMatchesKeyEventResult(0, KeyEvent.KEY_RELEASED, KeyEvent.VK_S, 's'));
    }

    KeyEvent createKeyEvent(int eventId, int keyCode, char keyChar) {
        KeyEvent event = new KeyEvent(
            new Canvas(),
            eventId,
            System.currentTimeMillis(),
            0,
            keyCode,
            keyChar
        );

        return event;
    }
    
    Handler createHandler() {
        Handler handler = new Handler();
        PlayerObject player = (PlayerObject) AbstractObjectsFactory.getInstance().createObject("Player");
        
        player.setX(Game.WIDTH/2 - 32);
        player.setY(Game.HEIGHT/2 - 32);

        handler.addObject(player);
        handler.setPlayer(player);

        return handler;
    }
    
    boolean velocityXMatchesKeyEventResult(int velocity, int eventId, int keyCode, char keyChar) {
        Handler handler = createHandler();
        GameplayKeyListener listener = new GameplayKeyListener(handler);
        if (eventId == KeyEvent.KEY_PRESSED) {
            listener.keyPressed(createKeyEvent(eventId, keyCode, keyChar));
        } else {
            listener.keyReleased(createKeyEvent(eventId, keyCode, keyChar));
        }

        return handler.getPlayer().getVelocityX() == velocity;
    }

    boolean velocityYMatchesKeyEventResult(int velocity, int eventId, int keyCode, char keyChar) {
        Handler handler = createHandler();
        GameplayKeyListener listener = new GameplayKeyListener(handler);
        if (eventId == KeyEvent.KEY_PRESSED) {
            listener.keyPressed(createKeyEvent(eventId, keyCode, keyChar));
        } else {
            listener.keyReleased(createKeyEvent(eventId, keyCode, keyChar));
        }

        return handler.getPlayer().getVelocityY() == velocity;
    }
}
