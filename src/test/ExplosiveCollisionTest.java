package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;

import objects.CollisionResult;
import objects.GameObject;

public class ExplosiveCollisionTest {
	@BeforeAll
	static void init() {
		AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
	}

	@Test
	void shouldIncreasePlayerAgilityWhenCollidingWithBonus() {
		GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
		assertTrue(explosive.collideWithBonus() == CollisionResult.IncreasePlayerAgility);
	}

	@Test
	void shouldExplodeObjectsWhenCollidingWithEnemy() {
		GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
		assertTrue(explosive.collideWithEnemy() == CollisionResult.ExplodeObjects);
	}

	@Test
	void shouldRefillFuelTankWhenCollidingWithFuelContainer() {
		GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
		assertTrue(explosive.collideWithFuelContainer() == CollisionResult.RefillFuelTank);
	}

	@Test
	void shouldExplodeObjectsWhenCollidingWithSpaceship() {
		GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
		assertTrue(explosive.collideWithSpaceship() == CollisionResult.ExplodeObjects);
	}

	@Test
	void shouldRemoveFirstObjectWhenCollidingWithWall() {
		GameObject explosive = AbstractObjectsFactory.getInstance().createObject("Explosive");
		assertTrue(explosive.collideWithWall() == CollisionResult.RemoveFirstObject);
	}
}
