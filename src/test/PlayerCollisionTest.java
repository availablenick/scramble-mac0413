package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;

import objects.CollisionResult;
import objects.GameObject;

public class PlayerCollisionTest {
	@BeforeAll
	static void init() {
		AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
	}

	@Test
	void shouldIncreasePlayerAgilityWhenCollidingWithBonus() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithBonus() == CollisionResult.IncreasePlayerAgility);
	}

	@Test
	void shouldRemoveSecondObjectWhenCollidingWithCheckpoint() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithCheckpoint() == CollisionResult.RemoveSecondObject);
	}

	@Test
	void shouldTakePlayerLifeWhenCollidingWithEnemy() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithEnemy() == CollisionResult.TakePlayerLife);
	}

	@Test
	void shouldTakePlayerLifeWhenCollidingWithFireball() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithFireball() == CollisionResult.TakePlayerLife);
	}

	@Test
	void shouldTakePlayerLifeWhenCollidingWithFuelContainer() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithFuelContainer() == CollisionResult.TakePlayerLife);
	}

	@Test
	void shouldTakePlayerLifeWhenCollidingWithSpaceship() {
		GameObject player = AbstractObjectsFactory.getInstance().createObject("Player");
		assertTrue(player.collideWithSpaceship() == CollisionResult.TakePlayerLife);
	}
}
