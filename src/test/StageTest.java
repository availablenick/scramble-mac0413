package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import factories.AbstractObjectsFactory;
import factories.SpaceObjectsFactory;

import stages.GameStage;
import stages.Stage1;
import stages.Stage2;
import stages.Stage3;
import stages.Stage4;
import stages.Stage5;

public class StageTest {
	@BeforeAll
	static void init() {
		AbstractObjectsFactory.setInstance(SpaceObjectsFactory.getInstance());
	}

	@Test
	void shouldCreateObjectsWhenInitializingStage1() {
		GameStage stage = new Stage1();
		checkStage(stage);
	}

	@Test
	void shouldCreateObjectsWhenInitializingStage2() {
		GameStage stage = new Stage2();
		checkStage(stage);
	}

	@Test
	void shouldCreateObjectsWhenInitializingStage3() {
		GameStage stage = new Stage3();
		checkStage(stage);
	}

	@Test
	void shouldCreateObjectsWhenInitializingStage4() {
		GameStage stage = new Stage4();
		checkStage(stage);
	}

	@Test
	void shouldCreateObjectsWhenInitializingStage5() {
		GameStage stage = new Stage5();
		checkStage(stage);
	}

	void checkStage(GameStage stage) {
		if (stage.getHandler().getPlayer() != null) {
			fail("Player is not null after stage instantiation");
		}
		
		stage.setUp();

		if (stage.getHandler().getPlayer() == null) {
			fail("Player is null after stage initialization");
		}

		assertTrue(stage.getHandler().countObjects() > 0);
	}
}
