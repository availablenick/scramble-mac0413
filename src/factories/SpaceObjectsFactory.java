package factories;

import java.util.HashMap;
import java.util.Map;

import main.ID;
import objects.BonusObject;
import objects.CheckpointObject;
import objects.EnemyObject;
import objects.ExplosionObject;
import objects.ExplosiveObject;
import objects.FireballObject;
import objects.FuelBarObject;
import objects.FuelContainerObject;
import objects.GameBackground;
import objects.GameObject;
import objects.LifeObject;
import objects.PlayerObject;
import objects.ProjectileObject;
import objects.ScoreObject;
import objects.SpaceshipObject;
import objects.WallObject;

public class SpaceObjectsFactory extends AbstractObjectsFactory {
	private static SpaceObjectsFactory instance = null;
	private Map<String, GameObject> prototypes = new HashMap<>();

	private SpaceObjectsFactory() {
		String imagesPath = AbstractObjectsFactory.imagesPath + "space/";

		this.prototypes.put("Bonus", new BonusObject(ID.BonusObject, 0, 0, -1, 0, imagesPath + "bonus.png"));
		this.prototypes.put("Checkpoint", new CheckpointObject(ID.CheckpointObject, 0, 0, 0, 0, imagesPath + "checkpoint.png"));
		this.prototypes.put("Enemy", new EnemyObject(ID.EnemyObject, 0, 0, -1, 0, imagesPath + "enemy.png"));
		this.prototypes.put("Explosive", new ExplosiveObject(ID.ExplosiveObject, 0, 0, 0, 0, imagesPath + "explosive.png"));
		this.prototypes.put("FuelBar", new FuelBarObject(ID.FuelBarObject, 0, 0, 0, 0, ""));
		this.prototypes.put("FuelContainer", new FuelContainerObject(ID.FuelContainerObject, 0, 0, 0, 0, imagesPath + "fuel-container.png"));
		this.prototypes.put("Life", new LifeObject(ID.LifeObject, 0, 0, 0, 0, imagesPath + "life.png"));
		this.prototypes.put("Player", new PlayerObject(ID.PlayerObject, 0, 0, 0, 0, imagesPath + "player.png"));
		this.prototypes.put("Projectile", new ProjectileObject(ID.ProjectileObject, 0, 0, 0, 0, imagesPath + "projectile.png"));
		this.prototypes.put("Score", new ScoreObject(ID.ScoreObject, 0, 0, 0, 0, ""));
		this.prototypes.put("GameBackground", new GameBackground(ID.Background, 0, 0, 0, 0, imagesPath + "background.png"));
		this.prototypes.put("Fireball", new FireballObject(ID.FireballObject, 0, 0, 0, 0, imagesPath + "fireball.png"));
		this.prototypes.put("Spaceship", new SpaceshipObject(ID.SpaceshipObject, 0, 0, 0, 0, imagesPath + "spaceship.png"));
		this.prototypes.put("TopWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "top.png", "Vertical"));
		this.prototypes.put("TopMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "topmiddle.png", "Vertical"));
		this.prototypes.put("BottomMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "bottommiddle.png", "Vertical"));
		this.prototypes.put("BottomWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "bottom.png", "Vertical"));
		this.prototypes.put("BottomAscendingMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "middlebottomascending.png", "BottomAscendingMiddle"));
		this.prototypes.put("BottomDescendingMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "middlebottomdescending.png", "BottomDescendingMiddle"));
		this.prototypes.put("TopAscendingMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "middletopascending.png", "TopAscendingMiddle"));
		this.prototypes.put("TopDescendingMiddleWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "middletopdescending.png", "TopDescendingMiddle"));
		this.prototypes.put("BottomAscendingDiagonalWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "bottomascending.png", "BottomAscendingDiagonal"));
		this.prototypes.put("BottomDescendingDiagonalWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "bottomdescending.png", "BottomDescendingDiagonal"));
		this.prototypes.put("TopAscendingDiagonalWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "topascending.png", "TopAscendingDiagonal"));
		this.prototypes.put("TopDescendingDiagonalWall", new WallObject(ID.WallObject, 0, 0, -3, 0, imagesPath + "topdescending.png", "TopDescendingDiagonal"));
		this.prototypes.put("Explosion", new ExplosionObject(ID.ExplosionObject, 0, 0, 0, 0, imagesPath + "explosion.png"));
	}

	public static SpaceObjectsFactory getInstance() {
		if (instance == null) {
			instance = new SpaceObjectsFactory();
		}

		return instance;
	}

	@Override
	public GameObject createObject(String type) {
		return instance.prototypes.get(type).clone();
	}
}
