package factories;

import objects.GameObject;

public abstract class AbstractObjectsFactory {
	public static final String imagesPath = "./img/";

	public abstract GameObject createObject(String type);

	private static AbstractObjectsFactory instance = null;

	public static void setInstance(AbstractObjectsFactory factory) {
		instance = factory;
	}

	public static AbstractObjectsFactory getInstance() {
		return instance;
	}
}
