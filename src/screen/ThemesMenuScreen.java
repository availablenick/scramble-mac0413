package screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

import audio.AudioPlayer;
import audio.PitchDecoratorAudioPlayer;
import factories.AbstractObjectsFactory;
import factories.DoodleObjectsFactory;
import factories.SpaceObjectsFactory;
import factories.WesternObjectsFactory;
import main.Game;
import main.GameCanvas;
import objects.GameBackground;

public class ThemesMenuScreen extends Menu implements GameScreen {
    private static ThemesMenuScreen instance = null;
    private final Map<String, AbstractObjectsFactory> themeNameToFactoryMap;
    private final KeyListener keyListener;
    private GameCanvas canvas;

    private ThemesMenuScreen() {
        this.keyListener = new MenuKeyListener(this);
        this.selectedOption = 0;
        this.options = new String[]{"Space", "Western", "Doodle", "Back to Main Menu"};
        this.themeNameToFactoryMap = new HashMap<>();
        this.initMaps();
    }

    public static ThemesMenuScreen getInstance() {
        if (instance == null) {
            instance = new ThemesMenuScreen();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void render(Graphics g, GameBackground bg) {
    	bg.render(g);

        g.setColor(Color.red);
        g.setFont(this.titleFont);
        g.drawString(
            "Select Theme",
            this.calculateCenteredTextXCoordinate(g, g.getFont(), "Select Theme"),
            Math.round(Game.HEIGHT * ((float)1 / 10))
        );

        this.drawOptions(g);
    }

    @Override
    public void setUp() {
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
    }

    @Override
    public void executeOption() {
        if (this.selectedOption != 3) {
            String newTheme = this.options[this.selectedOption];
            AbstractObjectsFactory newFactory = this.themeNameToFactoryMap.get(newTheme);
            AbstractObjectsFactory.setInstance(newFactory);
            PitchDecoratorAudioPlayer.getInstance().changeTheme(newTheme);
            this.canvas.updateBackground();
        }

        this.canvas.setScreen(MainMenuScreen.getInstance());
        PitchDecoratorAudioPlayer.getInstance().playSound("MenuSelect");
    }

    private void initMaps() {
        this.themeNameToFactoryMap.put("Space", SpaceObjectsFactory.getInstance());
        this.themeNameToFactoryMap.put("Western", WesternObjectsFactory.getInstance());
        this.themeNameToFactoryMap.put("Doodle", DoodleObjectsFactory.getInstance());
    }

    private void drawOptions(Graphics g) {
        for (int i = 0; i < this.options.length; i++) {
            Font fontToUse = (i == this.selectedOption)
                ? this.selectedOptionFont
                : this.unselectedOptionFont;

            g.setFont(fontToUse);
            g.drawString(
                this.options[i],
                this.calculateCenteredTextXCoordinate(g, fontToUse, this.options[i]),
                Math.round(Game.HEIGHT * ((float)(2 + (i * 2))/ 10))
            );
        }
    }

    private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }
}
