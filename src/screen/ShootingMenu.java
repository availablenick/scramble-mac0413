package screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

import audio.PitchDecoratorAudioPlayer;
import main.Game;
import main.GameCanvas;
import objects.GameBackground;
import objects.ShootingContext;
import objects.ShootingNormal;
import objects.ShootingTrack;
import objects.ShootingWave;



public class ShootingMenu extends Menu implements GameScreen {
    private static ShootingMenu instance = null;
    private final Map<String, ShootingContext> nameToDifficulty;
    private final KeyListener keyListener;
    private GameCanvas canvas;


    private ShootingMenu() {
        this.keyListener = new MenuKeyListener(this);
        this.selectedOption = 0;
        this.options = new String[]{"Normal", "Hard", "Hell", "Back to Main Menu"};
        this.nameToDifficulty = new HashMap<>();
        this.initMap();
    }

    public static ShootingMenu getInstance() {
        if (instance == null) {
            instance = new ShootingMenu();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void render(Graphics g, GameBackground bg) {
    	bg.render(g);

        g.setColor(Color.red);
        g.setFont(this.titleFont);
        g.drawString(
            "Select Difficulty",
            this.calculateCenteredTextXCoordinate(g, g.getFont(), "Select Difficulty"),
            Math.round(Game.HEIGHT * ((float)1 / 10))
        );

        this.drawOptions(g);
    }

    @Override
    public void setUp() {
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
    }

    @Override
    public void executeOption() {
        if (this.selectedOption != 3) {
        	String newDifficulty = this.options[this.selectedOption];
        	ShootingContext ctx = this.nameToDifficulty.get(newDifficulty);
        	ShootingContext.setInstance(ctx);            
        }

        PitchDecoratorAudioPlayer.getInstance().playSound("MenuSelect");
        this.canvas.setScreen(MainMenuScreen.getInstance());
    }

    
    private void initMap() {
        this.nameToDifficulty.put("Normal", new ShootingContext(new ShootingNormal()));
        this.nameToDifficulty.put("Hard", new ShootingContext(new ShootingWave()));
        this.nameToDifficulty.put("Hell", new ShootingContext(new ShootingTrack()));
    }

    private void drawOptions(Graphics g) {
        for (int i = 0; i < this.options.length; i++) {
            Font fontToUse = (i == this.selectedOption)
                ? this.selectedOptionFont
                : this.unselectedOptionFont;

            g.setFont(fontToUse);
            g.drawString(
                this.options[i],
                this.calculateCenteredTextXCoordinate(g, fontToUse, this.options[i]),
                Math.round(Game.HEIGHT * ((float)(2 + (i * 2))/ 10))
            );
        }
    }

    private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }
}
