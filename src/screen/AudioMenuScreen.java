package screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;

import audio.PitchDecoratorAudioPlayer;
import main.Game;
import main.GameCanvas;
import objects.GameBackground;

public class AudioMenuScreen extends Menu implements GameScreen {
    private static AudioMenuScreen instance = null;
    private final KeyListener keyListener;
    private GameCanvas canvas;

    private AudioMenuScreen() {
        this.keyListener = new MenuKeyListener(this);
        this.selectedOption = 0;
        this.options = new String[]{"Volume", "Pitch", "Back to Main Menu"};
    }

    public static AudioMenuScreen getInstance() {
        if (instance == null) {
            instance = new AudioMenuScreen();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void render(Graphics g, GameBackground bg) {
    	bg.render(g);

        g.setColor(Color.red);
        g.setFont(this.titleFont);
        g.drawString(
            "Audio Options",
            this.calculateCenteredTextXCoordinate(g, g.getFont(), "Audio Options"),
            Math.round(Game.HEIGHT * ((float)1 / 10))
        );

        this.drawOptions(g);
    }

    @Override
    public void setUp() {
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
    }

    @Override
    public void executeOption() {
    	if (this.selectedOption == 0) {
    		this.canvas.setScreen(VolumeMenuScreen.getInstance());
    	} else if (this.selectedOption == 1) {
    		this.canvas.setScreen(PitchMenuScreen.getInstance());
    	}
    	else {
    		this.canvas.setScreen(MainMenuScreen.getInstance());
    	}
        PitchDecoratorAudioPlayer.getInstance().playSound("MenuSelect");
    }

    private void drawOptions(Graphics g) {
        for (int i = 0; i < this.options.length; i++) {
            Font fontToUse = (i == this.selectedOption)
                ? this.selectedOptionFont
                : this.unselectedOptionFont;

            g.setFont(fontToUse);
            g.drawString(
                this.options[i],
                this.calculateCenteredTextXCoordinate(g, fontToUse, this.options[i]),
                Math.round(Game.HEIGHT * ((float)(3 + (i * 2))/ 10))
            );
        }
    }

    private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }
}
