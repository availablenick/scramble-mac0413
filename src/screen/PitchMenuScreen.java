package screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;

import audio.PitchDecoratorAudioPlayer;
import main.Game;
import main.GameCanvas;
import objects.GameBackground;

public class PitchMenuScreen extends Menu implements GameScreen {
    private static PitchMenuScreen instance = null;
    private final KeyListener keyListener;
    private GameCanvas canvas;

    private PitchMenuScreen() {
        this.keyListener = new MenuKeyListener(this);
        this.selectedOption = 0;
        this.options = new String[]{"-5", "-4", "-3", "-2", "-1", "0", "1", "2", "3", "4", "5", "Back to Options"};
    }

    public static PitchMenuScreen getInstance() {
        if (instance == null) {
            instance = new PitchMenuScreen();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void render(Graphics g, GameBackground bg) {
    	bg.render(g);

        g.setColor(Color.red);
        g.setFont(this.titleFont);
        g.drawString(
            "Change Pitch",
            this.calculateCenteredTextXCoordinate(g, g.getFont(), "Change Pitch"),
            Math.round(Game.HEIGHT * ((float)1 / 10))
        );

        this.drawOptions(g);
    }

    @Override
    public void setUp() {
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
    }

    @Override
    public void executeOption() {
    	float pitch = 0.0f;
    	if (this.selectedOption == 11) {
    		this.canvas.setScreen(AudioMenuScreen.getInstance());
    	}
    	else {
	        pitch = (this.selectedOption - 5) * 0.2f + 1;
	        PitchDecoratorAudioPlayer.getInstance().setPitch(pitch);
    	}
	    PitchDecoratorAudioPlayer.getInstance().playSound("MenuSelect");
    }

    private void drawOptions(Graphics g) {
        for (int i = 0; i < this.options.length; i++) {
            Font fontToUse = (i == this.selectedOption)
                ? this.selectedOptionFont
                : this.unselectedOptionFont;

            g.setFont(fontToUse);
            g.drawString(
                this.options[i],
                this.calculateCenteredTextXCoordinate(g, fontToUse, this.options[i]),
                Math.round(Game.HEIGHT * ((float)(8 + (i * 3))/ 50))
            );
        }
    }

    private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }
}
