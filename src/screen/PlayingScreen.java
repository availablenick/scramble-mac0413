package screen;

import java.awt.Graphics;

import audio.PitchDecoratorAudioPlayer;
import main.GameCanvas;
import objects.GameBackground;
import stages.GameStage;

public class PlayingScreen implements GameScreen {
	private static PlayingScreen instance = null;
	private final GameplayKeyListener keyListener;
	private GameCanvas canvas;
	private GameStage stage;

	private PlayingScreen() {
		this.keyListener = new GameplayKeyListener();
	}

	public static PlayingScreen getInstance() {
        if (instance == null) {
            instance = new PlayingScreen();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

	public void setStage(GameStage stage) {
		this.stage = stage;
	}
	
	public void updateStage() {
		if (this.stage.hasNextStage) {
			GameStage nextStage = this.stage.nextStage();
			this.setStage(nextStage);
			PitchDecoratorAudioPlayer.getInstance().changeStage(nextStage.getStageName());
			this.setUp();
		} else {
			PitchDecoratorAudioPlayer.getInstance().changeStage("Menu");
			this.canvas.setScreen(MainMenuScreen.getInstance());
		}
	}

	@Override
	public void render(Graphics g, GameBackground bg){
		bg.render(g);
		this.stage.getHandler().render(g);
	}

	@Override
	public void tick() {
		this.stage.getHandler().tick();
		if (this.stage.getHandler().lifeCounter.getLifeCount() <= 0) {
			PitchDecoratorAudioPlayer.getInstance().changeStage("Menu");
			this.canvas.setScreen(MainMenuScreen.getInstance());
			return;
		}

		if (this.stage.getHandler().isStageFinished) {
			this.updateStage();
		}
	}

	@Override
    public void setUp() {
		this.stage.setUp();
		this.stage.getHandler().snapshot.update(this.stage.getHandler());
		this.keyListener.setHandler(this.stage.getHandler());
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
		this.stage.getHandler().removeAll();
    }
}
