package screen;

import java.awt.Font;

import audio.PitchDecoratorAudioPlayer;

public abstract class Menu {
    protected final Font titleFont = new Font(Font.MONOSPACED, Font.BOLD, 50);
    protected final Font selectedOptionFont = new Font(Font.MONOSPACED, Font.BOLD, 24);
    protected final Font unselectedOptionFont = new Font(Font.MONOSPACED, Font.BOLD, 20);

    protected String[] options;
    protected int selectedOption;

    public void goToNextOption() {
        this.selectedOption = (this.selectedOption + 1) % this.options.length;
        PitchDecoratorAudioPlayer.getInstance().playSound("MenuScroll");
    }

    public void goToPreviousOption() {
        this.selectedOption = (this.selectedOption - 1 + this.options.length) % this.options.length;
        PitchDecoratorAudioPlayer.getInstance().playSound("MenuScroll");
    }

    public abstract void executeOption();
}
