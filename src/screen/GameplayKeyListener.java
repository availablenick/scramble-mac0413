package screen;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import main.Handler;
import objects.ExplosiveObject;
import objects.PlayerObject;
import objects.ProjectileObject;

public class GameplayKeyListener extends KeyAdapter {
	private Handler handler;
	public final int VELOCITY = 4;
	
	public GameplayKeyListener() {}
	
	public GameplayKeyListener(Handler handler) {
	    this.handler = handler;
	}

	public void setHandler(Handler handler) {
		this.handler = handler;
		Handler.setInstance(handler);
	}

	public void keyPressed(KeyEvent e) {
		if (handler == null) return;

		int key = e.getKeyCode();
		
		PlayerObject player = handler.getPlayer();
		if (key == KeyEvent.VK_W) player.setVelocityY(-VELOCITY);
		else if (key == KeyEvent.VK_A) player.setVelocityX(-VELOCITY);
		else if (key == KeyEvent.VK_S) player.setVelocityY(VELOCITY);
		else if (key == KeyEvent.VK_D) player.setVelocityX(VELOCITY);
		
		else if (key == KeyEvent.VK_K) {
			ProjectileObject projectile = player.shoot();
			if (projectile != null) handler.addObject(projectile);
		}

		else if (key == KeyEvent.VK_L) {
			ExplosiveObject explosive = player.dropExplosive();
			if (explosive != null) handler.addObject(explosive);
		}
		else if (key == KeyEvent.VK_ESCAPE) System.exit(VELOCITY);
	}

	public void keyReleased(KeyEvent e) {
		if (handler == null) return;

		int key = e.getKeyCode();
		
		PlayerObject player = handler.getPlayer();	

		if (key == KeyEvent.VK_W) player.setVelocityY(0);
		else if (key == KeyEvent.VK_S) player.setVelocityY(0);
		else if (key == KeyEvent.VK_A) player.setVelocityX(0);
		else if (key == KeyEvent.VK_D) player.setVelocityX(0);
	}
}
