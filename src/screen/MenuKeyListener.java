package screen;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class MenuKeyListener extends KeyAdapter {
    private final Menu menu;

    public MenuKeyListener(Menu menu) {
        this.menu = menu;
    }

    public void keyPressed(KeyEvent e) {
        int key = e.getKeyCode();

        if (key == KeyEvent.VK_DOWN) this.menu.goToNextOption();
        else if (key == KeyEvent.VK_UP) this.menu.goToPreviousOption();
        else if (key == KeyEvent.VK_ENTER) this.menu.executeOption();
    }

    public void keyReleased(KeyEvent e) {}
}
