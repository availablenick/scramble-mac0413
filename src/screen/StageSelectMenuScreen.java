package screen;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.KeyListener;
import java.util.HashMap;
import java.util.Map;

import audio.PitchDecoratorAudioPlayer;
import main.Game;
import main.GameCanvas;
import objects.GameBackground;
import stages.GameStage;
import stages.Stage1;
import stages.Stage2;
import stages.Stage3;
import stages.Stage4;
import stages.Stage5;

public class StageSelectMenuScreen extends Menu implements GameScreen {
    private static StageSelectMenuScreen instance = null;
    private final Map<String, GameStage> nameToStageMap;
    private final KeyListener keyListener;
    private GameCanvas canvas;

    private StageSelectMenuScreen() {
        this.keyListener = new MenuKeyListener(this);
        this.selectedOption = 0;
        this.options = new String[]{"Stage 1", "Stage 2", "Stage 3", "Stage 4", "Stage 5", "Back to Main Menu"};
        this.nameToStageMap = new HashMap<>();
        this.initMap();
    }

    public static StageSelectMenuScreen getInstance() {
        if (instance == null) {
            instance = new StageSelectMenuScreen();
        }

        return instance;
    }

    public void setCanvas(GameCanvas canvas) {
        this.canvas = canvas;
    }

    @Override
    public void render(Graphics g, GameBackground bg) {
    	bg.render(g);

        g.setColor(Color.red);
        g.setFont(this.titleFont);
        g.drawString(
            "Select Stage",
            this.calculateCenteredTextXCoordinate(g, g.getFont(), "Select Stage"),
            Math.round(Game.HEIGHT * ((float)1 / 10))
        );

        this.drawOptions(g);
    }

    @Override
    public void setUp() {
        this.canvas.addKeyListener(this.keyListener);
    }

    @Override
    public void tearDown() {
        this.canvas.removeKeyListener(this.keyListener);
    }

    @Override
    public void executeOption() {
        if (this.selectedOption != 5) {
            String stageName = this.options[this.selectedOption];
            PlayingScreen.getInstance().setStage(createStage(stageName));
            PitchDecoratorAudioPlayer.getInstance().changeStage(stageName);
            this.canvas.setScreen(PlayingScreen.getInstance());
        } else {
        	this.canvas.setScreen(MainMenuScreen.getInstance());
        }
        PitchDecoratorAudioPlayer.getInstance().playSound("MenuSelect");
    }

    private GameStage createStage(String name) {
        switch (name) {
            case "Stage 1":
                return new Stage1();
            case "Stage 2":
                return new Stage2();
            case "Stage 3":
                return new Stage3();
            case "Stage 4":
                return new Stage4();
            case "Stage 5":
                return new Stage5();
            default:
                break;
        }

        return null;
    }
    
    private void initMap() {
        this.nameToStageMap.put("Stage 1", new Stage1());
        this.nameToStageMap.put("Stage 2", new Stage2());
        this.nameToStageMap.put("Stage 3", new Stage3());
        this.nameToStageMap.put("Stage 4", new Stage4());
        this.nameToStageMap.put("Stage 5", new Stage5());
    }

    private void drawOptions(Graphics g) {
        for (int i = 0; i < this.options.length; i++) {
            Font fontToUse = (i == this.selectedOption)
                ? this.selectedOptionFont
                : this.unselectedOptionFont;

            g.setFont(fontToUse);
            g.drawString(
                this.options[i],
                this.calculateCenteredTextXCoordinate(g, fontToUse, this.options[i]),
                Math.round(Game.HEIGHT * ((float)(4 + (i * 2))/ 17))
            );
        }
    }

    private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }
}
