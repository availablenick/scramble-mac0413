package screen;

import java.awt.Graphics;

import objects.GameBackground;

public interface GameScreen {
    public void render(Graphics g, GameBackground bg);

    default public void tick() {}

    public void setUp();

    public void tearDown();
}
