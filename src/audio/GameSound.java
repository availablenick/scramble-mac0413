package audio;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Sound;

public class GameSound extends Sound {
	
	private float volume;
	
	private float pitch;
	
	GameSound(String soundPath) throws SlickException {
		super(soundPath);
		this.volume = 1.0f;
		this.pitch = 1.0f;
	}

	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
	}

	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
	}
	
	@Override
	public void play() {
		play(pitch, volume);
	}
	
	@Override
	public void loop() {
		loop(pitch, volume);
	}

}
