package audio;

public abstract class BaseAudioPlayer {

	public abstract GameSound getSound(String soundName);
	
	public void playSound(String soundName) {
		GameSound sound = getSound(soundName);
		sound.play();
	}
	
	public abstract GameSound getBackgroundMusic();
	
	public void playBackgroundMusic() {
		GameSound sound = getBackgroundMusic();
		sound.loop();
	}
	
	public abstract void stopBackgroundMusic();
	
	public abstract void changeTheme(String newTheme);
	
	public abstract void changeStage(String newStage);
}
