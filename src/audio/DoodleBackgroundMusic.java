package audio;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.SlickException;

public class DoodleBackgroundMusic {
	
	private static String soundsPath = "./sounds/doodle/";
	
	public static Map<String, GameSound> getSoundMap() {
		Map<String, GameSound> soundMap = new HashMap<>();
		try {
			soundMap.put("Menu", new GameSound(soundsPath + "stage1.ogg"));
			soundMap.put("Stage 1", new GameSound(soundsPath + "stage1.ogg"));
			soundMap.put("Stage 2", new GameSound(soundsPath + "stage2.ogg"));
			soundMap.put("Stage 3", new GameSound(soundsPath + "stage3.ogg"));
			soundMap.put("Stage 4", new GameSound(soundsPath + "stage4.ogg"));
			soundMap.put("Stage 5", new GameSound(soundsPath + "stage5.ogg"));
		} catch (SlickException e) {
			e.printStackTrace();
		}
		return soundMap;
	}

}
