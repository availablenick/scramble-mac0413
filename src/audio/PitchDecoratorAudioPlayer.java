package audio;

public class PitchDecoratorAudioPlayer extends BaseAudioPlayer {
	
	private BaseAudioPlayer audioPlayer;
	
	private float pitch;
	
	private static PitchDecoratorAudioPlayer instance = null;
	
	PitchDecoratorAudioPlayer(){
		this.pitch = 1.0f;
	}
	
	public float getPitch() {
		return pitch;
	}

	public void setPitch(float pitch) {
		this.pitch = pitch;
		stopBackgroundMusic();
		playBackgroundMusic();
	}
	
	public void setAudioPlayer(BaseAudioPlayer audioPlayer) {
		this.audioPlayer = audioPlayer;
	}
	public static PitchDecoratorAudioPlayer getInstance() {
		if (instance == null) {
			instance = new PitchDecoratorAudioPlayer();
		}
		return instance;
	}	

	@Override
	public GameSound getSound(String soundName) {
		GameSound sound = audioPlayer.getSound(soundName);
		sound.setPitch(pitch);
		return sound;
	}
	
	@Override
	public GameSound getBackgroundMusic() {
		GameSound sound = audioPlayer.getBackgroundMusic();
		sound.setPitch(this.pitch);
		return sound;
	}
	

	@Override
	public void stopBackgroundMusic() {
		audioPlayer.stopBackgroundMusic();
	}

	@Override
	public void changeTheme(String newTheme) {
		audioPlayer.changeTheme(newTheme);
	}
	
	@Override
	public void changeStage(String newStage) {
		audioPlayer.changeStage(newStage);
	}

}
