package audio;

public class VolumeDecoratorAudioPlayer extends BaseAudioPlayer {
	
	private BaseAudioPlayer audioPlayer;
	
	private float volume;
	
	private static VolumeDecoratorAudioPlayer instance = null;
	
	VolumeDecoratorAudioPlayer(){
		this.volume = 1.0f;
	}
	
	public float getVolume() {
		return volume;
	}

	public void setVolume(float volume) {
		this.volume = volume;
		stopBackgroundMusic();
		playBackgroundMusic();
	}
	
	public void setAudioPlayer(BaseAudioPlayer audioPlayer) {
		this.audioPlayer = audioPlayer;
	}
	public static VolumeDecoratorAudioPlayer getInstance() {
		if (instance == null) {
			instance = new VolumeDecoratorAudioPlayer();
		}
		return instance;
	}	

	@Override
	public GameSound getSound(String soundName) {
		GameSound sound = audioPlayer.getSound(soundName);
		sound.setVolume(volume);
		return sound;
	}
	
	@Override
	public GameSound getBackgroundMusic() {
		GameSound sound = audioPlayer.getBackgroundMusic();
		// Som de fundo deve ficar mais baixo que o restante
		sound.setVolume(volume / 2);
		return sound;
	}
	
	@Override
	public void stopBackgroundMusic() {
		audioPlayer.stopBackgroundMusic();
	}

	@Override
	public void changeTheme(String newTheme) {
		audioPlayer.changeTheme(newTheme);
	}
	
	@Override
	public void changeStage(String newStage) {
		audioPlayer.changeStage(newStage);
	}

}
