package audio;

import java.util.HashMap;
import java.util.Map;

import org.newdawn.slick.SlickException;

public class AudioPlayer extends BaseAudioPlayer{
	
	public static final String soundsPath = "./sounds/";
	
	private Map<String, GameSound> soundMap = new HashMap<>();
	
	private Map<String, Map<String, GameSound>> themeToBackgroundMusicMap = new HashMap<>();
	
	private String theme = null;
	
	private String stage = null;
	
	private static AudioPlayer instance = null;
	
	private AudioPlayer() {
		
		theme = "Space";
		
		stage = "Menu";
		
		try {
			this.themeToBackgroundMusicMap.put("Space", SpaceBackgroundMusic.getSoundMap());
			this.themeToBackgroundMusicMap.put("Western", WesternBackgroundMusic.getSoundMap());
			this.themeToBackgroundMusicMap.put("Doodle", DoodleBackgroundMusic.getSoundMap());
			
			this.soundMap.put("Projectile", new GameSound(soundsPath + "projectile.ogg"));
			this.soundMap.put("Explosive", new GameSound(soundsPath + "explosive.ogg"));
			this.soundMap.put("Explosion", new GameSound(soundsPath + "explosion.ogg"));
			this.soundMap.put("Crash", new GameSound(soundsPath + "crash.ogg"));
			this.soundMap.put("MenuScroll", new GameSound(soundsPath + "menuscroll.ogg"));
			this.soundMap.put("MenuSelect", new GameSound(soundsPath + "menuselect.ogg"));
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}
	
	public static AudioPlayer getInstance() {
		if (instance == null) {
			instance = new AudioPlayer();
		}
		return instance;
	}	
	
	public GameSound getSound(String soundName) {		
		return this.soundMap.get(soundName);
	}

	public GameSound getBackgroundMusic() {
		GameSound sound = themeToBackgroundMusicMap.get(theme).get(stage);
		return sound;
	}
	
	public void stopBackgroundMusic() {
		GameSound sound = themeToBackgroundMusicMap.get(theme).get(stage);
		sound.stop();
	}
	
	public void changeTheme(String newTheme) {
		stopBackgroundMusic();
		theme = newTheme;
		playBackgroundMusic();
	}
	
	public void changeStage(String newStage) {
		stopBackgroundMusic();
		stage = newStage;
		playBackgroundMusic();
	}
}


