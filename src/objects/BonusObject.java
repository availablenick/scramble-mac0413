package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class BonusObject extends GameObject {
	
	public BonusObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 32;
	}

	public BonusObject(BonusObject source) {
		super(source);
		this.width = source.width;
		this.height = source.height;
	}

  	@Override
	public void tick() {
  		this.x += this.velocityX;
	}
  	
  	@Override
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawImage(this.image, this.x, this.y, width, height, null, null);
	}
 
  	@Override
	public BonusObject clone() {
		return new BonusObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.IncreasePlayerAgility;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.IncreasePlayerAgility;
	}
	
	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.IncreasePlayerAgility;
	}
}
