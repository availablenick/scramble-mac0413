package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class LifeObject extends GameObject {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;

	public LifeObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
	}

	public LifeObject(LifeObject source) {
		super(source);
	}

	@Override
	public void tick() {}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawImage(this.image, this.x, this.y, WIDTH, HEIGHT, null, null);
	}

	@Override
	public LifeObject clone() {
		return new LifeObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}
}
