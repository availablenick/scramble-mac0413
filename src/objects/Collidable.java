package objects;

public interface Collidable {
	
	default public CollisionResult collideWithBonus() {
    	return CollisionResult.DoNothing;
    }
	
    default public CollisionResult collideWithCheckpoint() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithEnemy() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithExplosive() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithFireball() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithFuelContainer() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithPlayer() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithProjectile() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithSpaceship() {
        return CollisionResult.DoNothing;
    }

    default public CollisionResult collideWithWall() {
        return CollisionResult.DoNothing;
    }
}
