package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


import main.ID;

public class FireballObject extends GameObject {
	public FireballObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 16;
	}

	public FireballObject(FireballObject source) {
		super(source);
		this.width = 32;
		this.height = 16;
	}

  	@Override
	public void tick() {
		this.x += this.velocityX; 
		this.y += this.velocityY;
	}

  	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, x, y, width, height, null, null);
	}
 
  	@Override
	public FireballObject clone() {
		return new FireballObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithFireball() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithWall() {
		return CollisionResult.RemoveFirstObject;
	}
}
