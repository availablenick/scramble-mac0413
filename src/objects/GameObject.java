package objects;

import java.awt.Graphics;
import java.awt.Shape;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import main.Game;
import main.ID;

public abstract class GameObject implements Cloneable, Collidable {
	protected ID id;
	protected int x,y;
	protected int width, height;
	protected int velocityX, velocityY;
	protected BufferedImage image;

	public GameObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		this.id = id;
		this.x = x;
		this.y = y;
		this.velocityX = velocityX;
		this.velocityY = velocityY;
		this.image = this.renderImage(imagePath);
	}

	public GameObject(GameObject source) {
		this.x = source.x;
		this.y = source.y;
		this.id = source.id;
		this.velocityX = source.velocityX;
		this.velocityY = source.velocityY;
		this.image = source.image;
	}
	
	public abstract void tick();
	public abstract void render(Graphics g);
	public abstract Shape getBounds();
	public abstract GameObject clone();
	
	public int getX() {
		return this.x;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public int getY() {
		return this.y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public ID getID() {
		return this.id;
	}

	public void setID(ID id) {
		this.id = id;
	}
	
	public int getVelocityX() {
		return this.velocityX;
	}

	public void setVelocityX(int velocityX) {
		this.velocityX = velocityX;
	}
	
	public BufferedImage getImage() {
	    return this.image;
	}
	
	public int getVelocityY() {
		return this.velocityY;
	}

	public void setVelocityY(int velocityY) {
		this.velocityY = velocityY;
	}

	private BufferedImage renderImage(String imagePath) {
		if (imagePath == "") {
			return null;
		}

		try {
			return ImageIO.read(new File(imagePath));
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	public CollisionResult collideWith(GameObject other) {
		
		if (other instanceof BonusObject) {
			return this.collideWithBonus();
		}
		if (other instanceof CheckpointObject) {
			return this.collideWithCheckpoint();
		}
		if (other instanceof EnemyObject) {
			return this.collideWithEnemy();
		}
		if (other instanceof ExplosiveObject) {
			return this.collideWithExplosive();
		}
		if (other instanceof FireballObject) {
			return this.collideWithFireball();
		}
		if (other instanceof FuelContainerObject) {
			return this.collideWithFuelContainer();
		}
		if (other instanceof PlayerObject) {
			return this.collideWithPlayer();
		}
		if (other instanceof ProjectileObject) {
			return this.collideWithProjectile();
		}
		if (other instanceof SpaceshipObject) {
			return this.collideWithSpaceship();
		}
		if (other instanceof WallObject) {
			return this.collideWithWall();
		}
		return CollisionResult.DoNothing;
	}
}
