package objects;

public interface ShootingStrategy {
    public int shootingX(int x, int y, int velX);
    public int shootingY(int x, int y, int velY);
    
}
