package objects;



public class ShootingContext {
	
	private ShootingStrategy strategy;
	private static ShootingContext instance = null;
	
	public ShootingContext(ShootingStrategy strategy){
		this.strategy = strategy;
    }
   
	public static ShootingContext getInstance() {
		return instance;
	}
   
	public static void setInstance(ShootingContext ctx) {
		instance = ctx;
	}
   

	public int executeStrategyX(int x, int y, int velocityX) {
      return strategy.shootingX(x,y,velocityX);
   }
   
   public int executeStrategyY(int x, int y, int velocityY){
	      return strategy.shootingY(x,y,velocityY);
   }


	   
}
