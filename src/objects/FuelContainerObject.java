package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class FuelContainerObject extends GameObject {
	public static final int WIDTH = 32;
	public static final int HEIGHT = 32;

	public FuelContainerObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
	}

	public FuelContainerObject(FuelContainerObject source) {
		super(source);
	}

	@Override
	public void tick() {
		this.x += this.velocityX;
		this.y += this.velocityY;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawImage(this.image, this.x, this.y, WIDTH, HEIGHT, null, null);
	}

	@Override
	public FuelContainerObject clone() {
		return new FuelContainerObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}

	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.RefillFuelTank;
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.RefillFuelTank;
	}
}
