package objects;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.Game;
import main.ID;

public class ScoreObject extends GameObject {
	public static final int WIDTH = 300;
	public static final int HEIGHT = 20;	
	protected final Font displayFont = new Font(Font.MONOSPACED, Font.BOLD, 28);
	
	private int score;

	public ScoreObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);

		this.score = 0;
	}

	public ScoreObject(ScoreObject source) {
		super(source);

		this.score = source.score;
	}

	public void increaseScoreBy(int total) {
		this.score += total;
	}

	public void resetScore() {
		this.score = 0;
	}

	public int getScore() {
		return this.score;
	}

	@Override
	public void tick() {}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.setFont(this.displayFont);
		g.drawString(String.valueOf(this.score),
			this.calculateCenteredTextXCoordinate(g, g.getFont(), String.valueOf(this.score)),
			30);
	}
	
	private int calculateCenteredTextXCoordinate(Graphics g, Font font, String text) {
        return ((Game.WIDTH - g.getFontMetrics(font).stringWidth(text)) / 2);
    }

	@Override
	public ScoreObject clone() {
		return new ScoreObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}
}
