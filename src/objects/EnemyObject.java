package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

import main.Game;
import main.ID;


public class EnemyObject extends GameObject implements Scorable {
	Random r = new Random();
	private int jumpPosition;

	public EnemyObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 32;
	}

	public EnemyObject(EnemyObject source) {
		super(source);
		this.width = source.width;
		this.height = source.height;
		this.jumpPosition = source.jumpPosition;
	}
		

  	@Override
	public void tick() {
  		this.x += ShootingContext.getInstance().executeStrategyX(this.x,this.y,this.velocityX);

		if (this.x > Game.WIDTH )
			jumpPosition = r.nextInt(10);
		if(x<= 64*jumpPosition) {
			this.y += ShootingContext.getInstance().executeStrategyY(this.x,this.y,this.velocityY);
		}
	}
  	
  	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, x, y, width, height, null, null);
	}
 
  	@Override
	public EnemyObject clone() {
		return new EnemyObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}

	@Override
	public int getScoreValue() {
		return 10;
	}

	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.ExplodeObjects;
	}
}
