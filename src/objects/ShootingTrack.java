package objects;

import main.Game;
import main.Handler;

public class ShootingTrack implements ShootingStrategy {

	@Override
	public int shootingX(int x, int y, int velX) {
		return velX;
	}

	@Override
	public int shootingY(int x, int y, int velY) {
		int handlerX, handlerY ;
		handlerX = Handler.getInstance().getPlayer().getX();
		handlerY = Handler.getInstance().getPlayer().getY();
		
		if (x <= Game.WIDTH) {
			float diffY = y - handlerY;
			float distance = (float) Math.sqrt((x-handlerX)*(x-handlerX) + (y - handlerY)*(y - handlerY));
			return (int) Math.round((-10.0/distance) * diffY);
		}else {
			return 0;
		}
	}


}