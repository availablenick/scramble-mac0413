package objects;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;

import audio.PitchDecoratorAudioPlayer;
import main.ID;

public class ExplosionObject extends GameObject {
	private float alpha;

	public ExplosionObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 16;
		this.alpha = 1;
	}

	public ExplosionObject(ExplosionObject source) {
		super(source);
		this.width = 32;
		this.height = 16;
		this.alpha = 1;
	}

	public float getAlpha() {
		return this.alpha;
	}

  	@Override
	public void tick() {
		this.alpha = this.alpha - 0.05f > 0 ? this.alpha - 0.05f : 0;
	}

  	@Override
	public void render(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, alpha));
		g2d.setColor(Color.RED);
		g2d.drawImage(this.image, x, y, width, height, null, null);
		g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 1));
	}
 
  	@Override
	public ExplosionObject clone() {
  		PitchDecoratorAudioPlayer.getInstance().playSound("Explosion");
		return new ExplosionObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
}
