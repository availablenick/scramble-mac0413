package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class FuelBarObject extends GameObject {
	public static final int WIDTH = 20;
	public static final int HEIGHT = 100;
	private final int START_FUEL_AMOUNT = 2000;
	private int fuelAmount;

	public FuelBarObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);

		this.fuelAmount = START_FUEL_AMOUNT;
	}

	public FuelBarObject(FuelBarObject source) {
		super(source);

		this.fuelAmount = source.fuelAmount;
	}

	public void resetFuelAmount() {
		this.fuelAmount = START_FUEL_AMOUNT;
	}

	public int getFuelAmount() {
		return this.fuelAmount;
	}

	@Override
	public void tick() {
		this.decrementFuelAmount();
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.red);
		g.drawRect(this.x, this.y, WIDTH, HEIGHT);

		int total = HEIGHT - 2;
		int remainingFuelBarHeight = (int) ((HEIGHT - 2) * this.getRemainingFuelPercentage());
		int deltaY = total - remainingFuelBarHeight;

		g.setColor(Color.yellow);
		g.fillRect(this.x + 1, this.y + 1 + deltaY, WIDTH - 2, remainingFuelBarHeight);
	}

	@Override
	public FuelBarObject clone() {
		return new FuelBarObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}

	private void decrementFuelAmount() {
		this.fuelAmount--;
		if (this.fuelAmount < 0) {
			this.fuelAmount = 0;
		}
	}

	private double getRemainingFuelPercentage() {
		return ((double) this.fuelAmount / START_FUEL_AMOUNT);
	}
}
