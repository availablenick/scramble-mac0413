package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.Game;
import main.ID;

public class SpaceshipObject extends GameObject implements Scorable {
	public SpaceshipObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 32;
	}

	public SpaceshipObject(SpaceshipObject source) {
		super(source);
		this.width = 32;
		this.height = 32;
	}

  	@Override
	public void tick() {
  		this.x += this.velocityX; 
  		
  		if(this.x <= Game.WIDTH) {
  			this.y +=  3*Math.sin(this.x/10);
  		}	
  	}

  	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, x, y, width, height, null, null);
	}
 
  	@Override
	public SpaceshipObject clone() {
		return new SpaceshipObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	@Override
	public int getScoreValue() {
		return 30;
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithFireball() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithWall() {
		return CollisionResult.DoNothing;
	}
}
