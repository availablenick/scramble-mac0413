package objects;


import main.Game;

public class ShootingWave implements ShootingStrategy {
	
	@Override
	public int shootingX(int x, int y, int velX) {		
		
  		if(x <= Game.WIDTH && (y != 400 && y!= 320 && y!= 280)) {
  			velX +=  -5*Math.sin(y/10) + velX;
  			
  		}
  			return velX;
	}

	@Override
	public int shootingY(int x, int y, int velY) {
		return -7 + velY;
	}
}