package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Path2D;

import main.ID;

public class WallObject extends GameObject {
	
	private String wallType;
	
	public WallObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath, String wallType) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.wallType = wallType;
		this.width = 32;
		this.height = 32;
	}

	public WallObject(WallObject source) {
		super(source);
		this.wallType = source.wallType;
		this.width = 32;
		this.height = 32;
	}

  	@Override
	public void tick() {
		this.x += this.velocityX; 
		this.y += this.velocityY;
	}

  	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, this.x, this.y, this.width, this.height, null, null);
	}
 
  	@Override
	public WallObject clone() {
		return new WallObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.width, this.height);
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.DoNothing;
	}

	@Override
	public CollisionResult collideWithExplosive() {
		return CollisionResult.RemoveSecondObject;
	}

	@Override
	public CollisionResult collideWithFireball() {
		return CollisionResult.RemoveSecondObject;
	}

	@Override
	public CollisionResult collideWithPlayer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithProjectile() {
		return CollisionResult.RemoveSecondObject;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.RemoveSecondObject;
	}

	@Override
	public CollisionResult collideWithWall() {
		return CollisionResult.DoNothing;
	}
}
