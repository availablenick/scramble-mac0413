package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class ProjectileObject extends GameObject {
	public ProjectileObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 10;
		this.height = 5;
	}
  
	public ProjectileObject(ProjectileObject source) {
		super(source);
	}

	@Override
	public void tick() {
		this.x += this.velocityX;
		this.width = 10;
		this.height = 5;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, x, y, width, height, null, null);
	}
		
	@Override
	public ProjectileObject clone() {
		return new ProjectileObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	@Override
	public CollisionResult collideWithBonus() {
		return CollisionResult.IncreasePlayerAgility;
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithFuelContainer() {
		return CollisionResult.RefillFuelTank;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithWall() {
		return CollisionResult.RemoveFirstObject;
	}
}
