package objects;

public interface Scorable {
	public int getScoreValue();
}
