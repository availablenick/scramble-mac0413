package objects;

public enum CollisionResult {
    DoNothing,
    ExplodeObjects,
    IncreasePlayerAgility,
    RefillFuelTank,
    RemoveFirstObject,
    RemoveSecondObject,
    SetCheckpoint,
    TakePlayerLife
}
