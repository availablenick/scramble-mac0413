package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import factories.AbstractObjectsFactory;

import main.ID;

public class LifeCounterObject extends GameObject {
	public static final int WIDTH = 16;
	public static final int HEIGHT = 16;
	private final int START_LIFE_COUNT = 5;
	private int lifeCount;

	public LifeCounterObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.lifeCount = START_LIFE_COUNT;
	}

	public LifeCounterObject(LifeCounterObject source) {
		super(source);
		this.lifeCount = source.lifeCount;
	}

	public void decrementLifeCount() {
		this.lifeCount--;
		if (this.lifeCount < 0) {
			this.lifeCount = 0;
		}
	}

	public int getLifeCount() {
		return this.lifeCount;
	}

	@Override
	public void tick() {}

	@Override
	public void render(Graphics g) {
		for (int i = 0; i < this.lifeCount; i++) {
			GameObject life = AbstractObjectsFactory.getInstance().createObject("Life");
			life.setX(this.x + (24 * i));
			life.setY(this.y);
			life.render(g);
		}
	}

	@Override
	public LifeCounterObject clone() {
		return new LifeCounterObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}
}
