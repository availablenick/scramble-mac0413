package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.Game;
import main.ID;

public class GameBackground extends GameObject {
	public static final int WIDTH = 640;
	public static final int HEIGHT = 480;

	public GameBackground(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
	}

	public GameBackground(GameBackground source) {
		super(source);
	}

	@Override
	public void tick() {
		this.x += this.velocityX;
		this.y += this.velocityY;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.BLACK);
        g.fillRect(0, 0, Game.WIDTH, Game.HEIGHT);
        
		g.setColor(Color.BLACK);
		g.drawImage(this.image, 0, 0, WIDTH, HEIGHT, null, null);
	}

	@Override
    public GameBackground clone() {
		return new GameBackground(this);
    }

	@Override
	public Rectangle getBounds() {
		return null;
	}
}
