package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class ExplosiveObject extends GameObject {
	public ExplosiveObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 16;
		this.height = 16;
	}

	public ExplosiveObject(ExplosiveObject source) {
		super(source);
		this.width = 16;
		this.height = 16;
	}

	@Override
	public void tick() {
		this.x += this.velocityX;
		this.y += this.velocityY;
		if (this.velocityX > 1) {
			this.velocityX -= 2;
		} else if (this.velocityX == 1) {
			this.velocityX -= 1;
		}

		if (this.velocityY < 5) {
			this.velocityY += 1;
		}
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.drawImage(this.image, x, y, width, height, null, null);
	}

	@Override
	public ExplosiveObject clone() {
		return new ExplosiveObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x, y, width, height);
	}
	
	@Override
	public CollisionResult collideWithBonus() {
		return CollisionResult.IncreasePlayerAgility;
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithFuelContainer() {
		return CollisionResult.RefillFuelTank;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.ExplodeObjects;
	}

	@Override
	public CollisionResult collideWithWall() {
		return CollisionResult.RemoveFirstObject;
	}
}
