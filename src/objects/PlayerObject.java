package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import audio.PitchDecoratorAudioPlayer;
import factories.AbstractObjectsFactory;
import main.Game;
import main.ID;

public class PlayerObject extends GameObject {
	public static final int WIDTH = 48;
	public static final int HEIGHT = 48;
	private final int GUN_RELOAD_TIME = 10;
	private final int CANNON_RELOAD_TIME = 25;
	private int ticksToReloadCannon;
	private int ticksToReloadGun;
	private double BONUS_VELOCITY;

	public PlayerObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);

		this.width = 48;
		this.height = 48;
		this.ticksToReloadCannon = 0;
		this.ticksToReloadGun = 0;
		this.BONUS_VELOCITY= 1;
	}

	public PlayerObject(PlayerObject source) {
		super(source);

		this.width = source.width;
		this.height = source.height;
		this.BONUS_VELOCITY= source.BONUS_VELOCITY;
		this.ticksToReloadCannon = 0;
		this.ticksToReloadGun = 0;
		
	}
	
	public void setBonusVelocity(double bonusVelocity) {
		this.BONUS_VELOCITY = bonusVelocity;
	}

	public double getBonusVelocity(){
		return this.BONUS_VELOCITY;
	}
	
	@Override
	public void tick() {
		this.x += this.velocityX * this.BONUS_VELOCITY;
		this.y += this.velocityY * this.BONUS_VELOCITY;
		this.x = clamp(this.x, 0, Game.WIDTH - 50);
		this.y = clamp(this.y, 0, Game.HEIGHT - 72);
		if (!this.isCannonLoaded()) this.ticksToReloadCannon -= 1;
		if (!this.isGunLoaded()) this.ticksToReloadGun -= 1;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawImage(this.image, this.x, this.y, width, height, null, null);
	}
	
	@Override
	public PlayerObject clone() {
		return new PlayerObject(this);
	}

	public boolean isGunLoaded() {
		return this.ticksToReloadGun == 0;
	}

	public boolean isCannonLoaded() {
		return this.ticksToReloadCannon == 0;
	}

	public ProjectileObject shoot() {
		ProjectileObject projectile = null;
		if (this.isGunLoaded()) {
			this.ticksToReloadGun = this.GUN_RELOAD_TIME;
			projectile = (ProjectileObject) AbstractObjectsFactory.getInstance().createObject("Projectile");
			projectile.setX(this.x + width - 8);
			projectile.setY(this.y + height / 2);
			projectile.setVelocityX((int) (5 * this.BONUS_VELOCITY));
			PitchDecoratorAudioPlayer.getInstance().playSound("Projectile");
		}

		return projectile;
	}

	public ExplosiveObject dropExplosive(){
		ExplosiveObject explosive = null;
		if (this.isCannonLoaded()) {
			this.ticksToReloadCannon = this.CANNON_RELOAD_TIME;
			explosive = (ExplosiveObject) AbstractObjectsFactory.getInstance().createObject("Explosive");
			explosive.setX(this.x + width - 16);
			explosive.setY(this.y + height - 16);
			explosive.setVelocityX(20);
			explosive.setVelocityY(1);
			PitchDecoratorAudioPlayer.getInstance().playSound("Explosive");
		}

		return explosive;
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(x + 2, y + 2, width - 4, height - 4);
	}

	private int clamp(int value, int min, int max) {
		if (value > max) { 
			return value = max;
		} else if (value <= min) {
			return value = min;
		} else {
			return value;
		}
	}
	
	@Override
	public CollisionResult collideWithBonus() {
		return CollisionResult.IncreasePlayerAgility;
	}

	@Override
	public CollisionResult collideWithCheckpoint() {
		return CollisionResult.RemoveSecondObject;
	}

	@Override
	public CollisionResult collideWithEnemy() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithFireball() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithFuelContainer() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithSpaceship() {
		return CollisionResult.TakePlayerLife;
	}

	@Override
	public CollisionResult collideWithWall() {
		PitchDecoratorAudioPlayer.getInstance().playSound("Crash");
		return CollisionResult.TakePlayerLife;
	}
}
