package objects;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

import main.ID;

public class CheckpointObject extends GameObject implements Collidable {
	public static final int WIDTH = 32;
	public static final int HEIGHT = 32;

	public CheckpointObject(ID id, int x, int y, int velocityX, int velocityY, String imagePath) {
		super(id, x, y, velocityX, velocityY, imagePath);
		this.width = 32;
		this.height = 32;
	}

	public CheckpointObject(CheckpointObject source) {
		super(source);
		this.width = source.width;
		this.height = source.height;
	}

	@Override
	public void tick() {
		this.x += this.velocityX;
	}

	@Override
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.drawImage(this.image, this.x, this.y, width, height, null, null);
	}

	@Override
	public CheckpointObject clone() {
		return new CheckpointObject(this);
	}

	@Override
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, WIDTH, HEIGHT);
	}
}
