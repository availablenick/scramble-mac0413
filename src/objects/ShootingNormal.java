package objects;

public class ShootingNormal implements ShootingStrategy {
	
	@Override
	public int shootingX(int x, int y, int velX) {		
		return velX;
	}

	@Override
	public int shootingY(int x, int y, int velY) {

		return -7 + velY;
	}
}