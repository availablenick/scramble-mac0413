# Scramble

1. [Pré-requisitos](#pré-requisitos)
2. [Como compilar](#como-compilar)
3. [Como executar](#como-executar)
4. [Sobre o projeto](#sobre-o-projeto)
5. [Fase 1](#fase-1)


## Como compilar
Execute o comando:
```
$ javac -d bin -cp libs/*:. src/*/*.java
```

## Como executar
Execute o comando:
```
$ java -Djava.library.path=./libs/lwjgl/native/linux -Dfile.encoding=UTF-8 -classpath ./bin:./libs/jinput.jar:./libs/jogg-0.0.7.jar:./libs/jorbis-0.0.15.jar:./libs/lwjgl.jar:./libs/slick.jar main.Game
```

## Como rodar os testes
Execute o comando:
```
$ java -jar libs/junit-platform-console-standalone-1.8.1.jar --classpath bin/ --scan-class-path
```

## Sobre o projeto
Projeto do jogo Scramble para a disciplina MAC0413 - Tópicos Avançados de Programação Orientada a Objetos ministrada pelo Prof. Fábio Kon.

Membros do grupo:
- André de Almeida Marques
- João Gabriel Loureiro de Lima Lembo
- Leonardo Alves Pereira
- Victor Pereira Lima

A estrutura básica do projeto foi inspirada na série de vídeos disponível em: https://www.youtube.com/playlist?list=PLWms45O3n--6TvZmtFHaCWRZwEqnz2MHa

## Fase 1
Na primeira fase foram criados os seguintes elementos:
- Estrutura do jogo;
- Movimentação e ações do jogador através de entradas do teclado (requisito 1);
- Menu para iniciar o jogo e escolher os temas (requisito 2);
- Fábricas para criação de objetos de acordo com o tema escolhido (requisito 2);
- Renderização dos elementos.

## Fase 2
Na segunda fase foram criados os seguintes elementos:
- Estrutura de fases e cinco fases do jogo (requisito 4);
- Criação de três tipos de inimigo, um deles sendo o que sai do chão (requisito 3);
- Criação das paredes das fases;
- Criação do sistema de colisão;

## Fase 3
Na terceira fase foram feitos os seguintes elementos:
- Implementação dos diferentes níveis de dificuldade dos inimigos (requisito 5)
- Preparação das funcionalidades do jogador (combustível, vidas e pontos)
- Restrição de tempo para projéteis e explosivos do jogador

## Fase 4
Na quarta fase foram feitos os seguintes elementos:
- Inserção dos sons e músicas do jogo (requisitos 6 e 7);
- Criação de bônus de velocidade (requisito 8);
- Adição de checkpoints nas fases;
